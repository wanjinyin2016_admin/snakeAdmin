SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `snake_sms_log`;
CREATE TABLE `snake_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号',
  `session_id` varchar(128) DEFAULT '' COMMENT 'session_id',
  `add_time` int(11) DEFAULT '0' COMMENT '发送时间',
  `code` varchar(10) DEFAULT '' COMMENT '验证码',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '发送状态,1:成功,0:失败',
  `msg` varchar(255) DEFAULT NULL COMMENT '短信内容',
  `scene` int(1) DEFAULT '0' COMMENT '发送场景,1:用户注册,2:找回密码,3:客户下单,4:客户支付,5:商家发货,6:身份验证',
  `error_msg` text COMMENT '发送短信异常内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('98','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527854786','95566','0','验证码95566，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('99','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527854885','67647','0','验证码67647，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('100','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527854968','42825','0','验证码42825，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('101','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527855126','43434','0','验证码43434，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('102','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527855866','28006','0','验证码28006，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('103','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527855883','81563','0','验证码81563，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('104','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527855893','23481','0','验证码23481，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('105','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527855902','84698','0','验证码84698，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('106','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527856621','67375','0','验证码67375，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('107','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527856637','95175','0','验证码95175，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('108','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527856703','25648','0','验证码25648，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('109','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527856704','14126','0','验证码14126，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('110','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857064','19790','0','验证码19790，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('111','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857246','61889','0','验证码61889，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('112','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857247','94012','0','验证码94012，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('113','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857465','17318','0','验证码17318，您正在进行身份验证，打死不要告诉别人哦！','1','include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('114','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857509','69584','0','验证码69584，您正在进行身份验证，打死不要告诉别人哦！','1','include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('115','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857567','95617','0','验证码95617，您正在进行身份验证，打死不要告诉别人哦！','1','include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('116','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857693','73367','0','验证码73367，您正在进行身份验证，打死不要告诉别人哦！','1','include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('117','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857704','25630','0','验证码25630，您正在进行身份验证，打死不要告诉别人哦！','1','include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('118','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857749','65980','1','验证码65980，您正在进行身份验证，打死不要告诉别人哦！','1','');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('119','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857757','32503','0','验证码32503，您正在进行身份验证，打死不要告诉别人哦！','1','触发分钟级流控Permits:1. Code: isv.BUSINESS_LIMIT_CONTROL');
insert into `snake_sms_log`(`id`,`mobile`,`session_id`,`add_time`,`code`,`status`,`msg`,`scene`,`error_msg`) values('120','15361698313','bv1tptv7s0g09plh16o7v5k9h0','1527857979','86221','1','验证码86221，您正在进行身份验证，打死不要告诉别人哦！','1','');
