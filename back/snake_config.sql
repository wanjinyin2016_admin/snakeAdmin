SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `snake_config`;
CREATE TABLE `snake_config` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `name` varchar(50) DEFAULT NULL COMMENT '配置的key键名',
  `value` varchar(512) DEFAULT NULL COMMENT '配置的val值',
  `inc_type` varchar(64) DEFAULT NULL COMMENT '配置分组',
  `desc` varchar(50) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('124','reg_integral','100','basic','注册送积分');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('125','regis_sms_enable','0','sms','开启注册短信验证');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('126','sms_time_out','300','sms','验证码过期时间');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('127','sms_appkey','LTAIWVPi3BO0yqsv','sms','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('128','sms_secretKey','RMXQbSUmU3olwKik1fLgluGkSsSe6G','sms','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('129','sms_product','电商之家','sms','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('130','record_no','粤ICP备16028635号-2','shop_info','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('131','store_name','电商之家','shop_info','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('132','store_keyword','电商服务','shop_info','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('133','contact','王总','shop_info','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('134','phone','15361698313','shop_info','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('135','address','深圳市 龙岗区','shop_info','');
insert into `snake_config`(`id`,`name`,`value`,`inc_type`,`desc`) values('136','qq','1437935165','shop_info','');
