SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `snake_user_level`;
CREATE TABLE `snake_user_level` (
  `level_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `level_name` varchar(30) DEFAULT NULL COMMENT '头衔名称',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '等级必要金额',
  `discount` smallint(4) DEFAULT '0' COMMENT '折扣',
  `describe` varchar(200) DEFAULT NULL COMMENT '头街 描述',
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

insert into `snake_user_level`(`level_id`,`level_name`,`amount`,`discount`,`describe`) values('1','注册会员','0.00','100','注册会员');
insert into `snake_user_level`(`level_id`,`level_name`,`amount`,`discount`,`describe`) values('2','铜牌会员','10000.00','98','铜牌会员');
insert into `snake_user_level`(`level_id`,`level_name`,`amount`,`discount`,`describe`) values('3','白银会员','30000.00','95','白银会员');
insert into `snake_user_level`(`level_id`,`level_name`,`amount`,`discount`,`describe`) values('4','黄金会员','50000.00','92','黄金会员');
insert into `snake_user_level`(`level_id`,`level_name`,`amount`,`discount`,`describe`) values('5','钻石会员','100000.00','90','钻石会员');
insert into `snake_user_level`(`level_id`,`level_name`,`amount`,`discount`,`describe`) values('6','超级VIP','200000.00','88','超级VIP');
