SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `snake_sms_template`;
CREATE TABLE `snake_sms_template` (
  `tpl_id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `sms_sign` varchar(50) NOT NULL DEFAULT '' COMMENT '短信签名',
  `sms_tpl_code` varchar(100) NOT NULL DEFAULT '' COMMENT '短信模板ID',
  `tpl_content` varchar(512) NOT NULL DEFAULT '' COMMENT '发送短信内容',
  `send_scene` varchar(100) NOT NULL DEFAULT '' COMMENT '短信发送场景',
  `add_time` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

insert into `snake_sms_template`(`tpl_id`,`sms_sign`,`sms_tpl_code`,`tpl_content`,`send_scene`,`add_time`) values('26','电商之家','SMS_136300059','验证码${code}，您正在进行身份验证，打死不要告诉别人哦！','1','1527816542');
insert into `snake_sms_template`(`tpl_id`,`sms_sign`,`sms_tpl_code`,`tpl_content`,`send_scene`,`add_time`) values('27','电商之家','SMS_136300055','验证码${code}，您正在尝试修改登录密码，请妥善保管账户信息。','2','1527816488');
