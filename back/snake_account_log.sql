SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `snake_account_log`;
CREATE TABLE `snake_account_log` (
  `log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `user_money` decimal(10,2) DEFAULT '0.00' COMMENT '用户金额',
  `frozen_money` decimal(10,2) DEFAULT '0.00' COMMENT '冻结金额',
  `pay_points` mediumint(9) NOT NULL DEFAULT '0' COMMENT '支付积分',
  `change_time` int(10) unsigned NOT NULL COMMENT '变动时间',
  `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `order_sn` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `order_id` int(10) DEFAULT NULL COMMENT '订单id',
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

insert into `snake_account_log`(`log_id`,`user_id`,`user_money`,`frozen_money`,`pay_points`,`change_time`,`desc`,`order_sn`,`order_id`) values('1','2876','0.00','0.00','100','1527862310','会员注册赠送积分','','0');
insert into `snake_account_log`(`log_id`,`user_id`,`user_money`,`frozen_money`,`pay_points`,`change_time`,`desc`,`order_sn`,`order_id`) values('2','2877','0.00','0.00','100','1527862785','会员注册赠送积分','','0');
insert into `snake_account_log`(`log_id`,`user_id`,`user_money`,`frozen_money`,`pay_points`,`change_time`,`desc`,`order_sn`,`order_id`) values('3','2878','0.00','0.00','100','1527863145','会员注册赠送积分','','0');
