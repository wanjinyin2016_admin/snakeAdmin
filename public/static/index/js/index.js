layui.use(['layer','laytpl', 'form', 'jquery'], function(){
  var layer = layui.layer;
  var form = layui.form;
  var laytpl = layui.laytpl;
  var $ = layui.jquery;
  var DISABLED = 'layui-btn-disabled';
  // user_login_or_no();
  


    form.verify({

    //我们既支持上述函数式的方式，也支持下述数组的形式
    //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
    pass: [
      /^[\S]{6,12}$/
      ,'密码必须6到12位，且不能出现空格'
    ]

  }); 
   
   $('.lazada-add-pack').on('click',function(){
	   window.location.href="/index/pack/packadd";
   });
   $('.lazada-my-pack').on('click',function(){
	   window.location.href="/index/pack/packlist";
   });

    //用户注册
    form.on('submit(register)', function(data){

    $.post(REGISTERURL, data.field, function(data, textStatus, xhr) {
       if (data.status==1) {
        layer.msg(data.msg, {
        icon: 1,
        time: 2000 //2秒关闭（如果不配置，默认是3秒）
      }, function(){
         window.location.href = "index/user/index";
      }); 
       }else{
        layer.msg(data.msg);
       }
       
    },'json');

    return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    
  });

  $('#verify').on('click', function(event) {
        $('#verify').attr('src', VERIFYURL);
    });

  //用户登录
    form.on('submit(login)', function(data){

    $.post(LOGINURL, data.field, function(data, textStatus, xhr) {
       if (data.status==1) {
        layer.msg(data.msg, {
        icon: 1,
        time: 2000 //2秒关闭（如果不配置，默认是3秒）
      }, function(){
         window.location.href = data.url;
      }); 
       }else{
        layer.msg(data.msg);
        $('#verify').attr('src', VERIFYURL);
       }
       
    },'json');

    return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    
  });

    //用户找回密码
    form.on('submit(repwd)', function(data){
    var AJAXURL = $('form').attr('action');
    $.post(AJAXURL, data.field, function(data, textStatus, xhr) {
      if (data.status==1) {
          layer.msg(data.msg, {
            icon: 1,
            time: 2000 //2秒关闭（如果不配置，默认是3秒）
          }, function(){
             if (data.url!='') {
              window.location.href = data.url;
             }
             
          }); 
       }else{
          layer.msg(data.msg);
          $('#verify').attr('src', VERIFYURL);
       }
       
    },'json');

    return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    
  });

  //余额充值
  form.on('submit(orderPay)', function(data){
    var account = $('#input_val').val();
    if (account<1) {
      layer.msg('充值金额不能低于1元');
      return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    }
    $('#recharge_form').submit(); 
  });

  $('#FLY-getvercode').on('click', function(event) {
       var mobile  = $('#L_phone').val();
       var vercode = $('#L_vercodeQuiz').val();
       if (!isPhoneNo(mobile)) {
          layer.msg('手机号码格式错误');
          return;
       }

        $.post(SMSURL, {mobile:mobile}, function(data, textStatus, xhr) {
            if (data.status==1) {
              layer.msg(data.msg);
              settime($('#FLY-getvercode'));
            }else{
              layer.msg(data.msg);
            }
        },'json');
    });

  $('#FLY-getvercodephone').on('click', function(event) {
       var mobile  = $('#L_phone').val();
       var vercode = $('#L_vercodeQuiz').val();
       if (!isPhoneNo(mobile)) {
          layer.msg('手机号码格式错误');
          return;
       }
       if (vercode=='') {
          layer.msg('请填写图形码');
          return;
       }
       $.post(VERCODEURL, {vercode:vercode}, function(data, textStatus, xhr) {
          if (data.status==0) {
            layer.msg(data.msg);
          }else{
            $.post(SMSURL, {mobile:mobile}, function(data, textStatus, xhr) {
                if (data.status==1) {
                  layer.msg(data.msg);
                  settime($(this));
                }else{
                  layer.msg(data.msg);
                }
            },'json');
          }
       },'json');
       
    });


    //表单提交
  form.on('submit(*)', function(data){
    var action = $(data.form).attr('action'), button = $(data.elem);
    $.post('set',data.field,function(obj){
      if (obj.status == 1) {
        layer.msg(obj.msg, {icon: 1},function(){
          location.href = obj.action;
        });
          
      }else{
        layer.msg(obj.msg, {icon: 5});
      }
    },'json');
    
    return false;
  });

  // 验证手机号
  function isPhoneNo(phone) {
      var pattern = /^1[34578]\d{9}$/;
      return pattern.test(phone);
  }

  var countdown=60; 
  function settime(obj) { //发送验证码倒计时
      if (countdown == 0) { 
          obj.attr('disabled',false); 
          obj.text("获取验证码");
          countdown = 60; 
          return;
      } else { 
        console.log(countdown);
          obj.attr('disabled',true);
          obj.text("重新发送(" + countdown + ")");
          countdown--; 
      } 
    setTimeout(function() { 
      settime(obj) }
      ,1000) 
  }



  //签到
  var tplSignin = ['{{# if(d.signed){ }}'
    ,'<button class="layui-btn layui-btn-disabled">今日已签到</button>'
    ,'<span>获得了<cite>{{ d.experience }}</cite>积分</span>'
  ,'{{# } else { }}'
    ,'<button class="layui-btn layui-btn-danger" id="LAY_signin">今日签到</button>'
    ,'<span>可获得<cite>{{ d.experience }}</cite>积分</span>'
  ,'{{# } }}'].join('')
  ,tplSigninDay = '已连续签到<cite>{{ d.days }}</cite>天'

  ,signRender = function(data){
    laytpl(tplSignin).render(data, function(html){
      elemSigninMain.html(html);
    });
    laytpl(tplSigninDay).render(data, function(html){
      elemSigninDays.html(html);
    });
  }

  ,elemSigninHelp = $('#LAY_signinHelp')
  ,elemSigninTop = $('#LAY_signinTop')
  ,elemSigninMain = $('.fly-signin-main')
  ,elemSigninDays = $('.fly-signin-days');
  
  if(elemSigninMain[0]){
    $.post(ajaxGetStatus,'', function(data, textStatus, xhr) {
      if(!data.data) return;
        signRender.token = data.data.token;
        signRender(data.data);
        if(!data.data) return;
    },'json');
  }
  $('body').on('click', '#LAY_signin', function(){
    var othis = $(this);
    if(othis.hasClass(DISABLED)) return;
    $.post(ajaxSignIn, {token: signRender.token || 1}, function(data, textStatus, xhr) {
        if (data.status==1) {
          signRender(data.data);
        }else{
          layer.msg(data.msg);
          othis.removeClass(DISABLED);
        }
        
    },'json');
  });

  //签到说明
  elemSigninHelp.on('click', function(){
    layer.open({
      type: 1
      ,title: '签到说明'
      ,area: '300px'
      ,shade: 0.8
      ,shadeClose: true
      ,content: ['<div class="layui-text" style="padding: 20px;">'
        ,'<blockquote class="layui-elem-quote">“签到”可获得平台积分，规则如下</blockquote>'
        ,'<table class="layui-table">'
          ,'<thead>'
            ,'<tr><th>连续签到天数</th><th>每天可获积分</th></tr>'
          ,'</thead>'
          ,'<tbody>'
            ,'<tr><td>＜5</td><td>10</td></tr>'
            ,'<tr><td>≥5</td><td>20</td></tr>'
          ,'</tbody>'
        ,'</table>'
        ,'<ul style="padding-top: 0; padding-bottom: 0;">'
          ,'<li>中间若有间隔，则连续天数重新计算</li>'
          ,'<li style="color: #FF5722;">不可利用程序自动签到，否则积分清零</li>'
        ,'</ul>'
      ,'</div>'].join('')
    });
  });

  //签到活跃榜
  // var tplSigninTop = ['{{# layui.each(d.data, function(index, item){ }}'
  //   ,'<li>'
  //     ,'<a href="/u/{{item.uid}}" target="_blank">'
  //       ,'<img src="{{item.user.avatar}}">'
  //       ,'<cite class="fly-link">{{item.user.username}}</cite>'
  //     ,'</a>'
  //     ,'{{# var date = new Date(item.time); if(d.index < 2){ }}'
  //       ,'<span class="fly-grey">签到于 {{ layui.laytpl.digit(date.getHours()) + ":" + layui.laytpl.digit(date.getMinutes()) + ":" + layui.laytpl.digit(date.getSeconds()) }}</span>'
  //     ,'{{# } else { }}'
  //       ,'<span class="fly-grey">已连续签到 <i>{{ item.days }}</i> 天</span>'
  //     ,'{{# } }}'
  //   ,'</li>'
  // ,'{{# }); }}'
  // ,'{{# if(d.data.length === 0) { }}'
  //   ,'{{# if(d.index < 2) { }}'
  //     ,'<li class="fly-none fly-grey">今天还没有人签到</li>'
  //   ,'{{# } else { }}'
  //     ,'<li class="fly-none fly-grey">还没有签到记录</li>'
  //   ,'{{# } }}'
  // ,'{{# } }}'].join('');

  // elemSigninTop.on('click', function(){
  //   var loadIndex = layer.load(1, {shade: 0.8});
  //   fly.json('/top/signin/', function(res){
  //     var tpl = $(['<div class="layui-tab layui-tab-brief" style="margin: 5px 0 0;">'
  //       ,'<ul class="layui-tab-title">'
  //         ,'<li class="layui-this">最新签到</li>'
  //         ,'<li>今日最快</li>'
  //         ,'<li>总签到榜</li>'
  //       ,'</ul>'
  //       ,'<div class="layui-tab-content fly-signin-list" id="LAY_signin_list">'
  //         ,'<ul class="layui-tab-item layui-show"></ul>'
  //         ,'<ul class="layui-tab-item">2</ul>'
  //         ,'<ul class="layui-tab-item">3</ul>'
  //       ,'</div>'
  //     ,'</div>'].join(''))
  //     ,signinItems = tpl.find('.layui-tab-item');

  //     layer.close(loadIndex);

  //     layui.each(signinItems, function(index, item){
  //       var html = laytpl(tplSigninTop).render({
  //         data: res.data[index]
  //         ,index: index
  //       });
  //       $(item).html(html);
  //     });

  //     layer.open({
  //       type: 1
  //       ,title: '签到活跃榜 - TOP 20'
  //       ,area: '300px'
  //       ,shade: 0.8
  //       ,shadeClose: true
  //       ,id: 'layer-pop-signintop'
  //       ,content: tpl.prop('outerHTML')
  //     });

  //   });
  // });



   //发送激活邮件
  function activate(email){
    $.post('/index/api/activate/', {email: email}, function(data, textStatus, xhr) {
      if(data.status === 1){
          layer.alert('已成功将激活链接发送到了您的邮箱，接受可能会稍有延迟，请注意查收。', {
            icon: 1
          });
        }else{
          layer.msg(data.msg);
          setTimeout(function() {
            window.location.href=data.url;
          }, 2000);
        };
    },'json');
  };

  $('#LAY-activate').on('click', function(){
      activate($(this).attr('email'));
  });


  /*******用户登录变化class****/
  // function user_login_or_no()
  // {
  //  var uname = getCookie('lazada_uname');
    
  //  if (uname == '') {
  //    $('.islogin').remove();
  //    $('.nologin').show();
  //  } else {
  //    $('.nologin').remove();
  //    $('.islogin').show();
  //    $('#LAY_header_avatar cite').html(decodeURIComponent(uname));
  //  }
  // }

  
  /*******回到顶部*******/ 

  var backButton=$('.layui-fixbar [lay-type=top]');  

  function backToTop() { 
      $('html,body').animate({  
          scrollTop: 0  
      }, 800);  
  }  
  backButton.on('click', backToTop);  
    
  $(window).on('scroll', function () {/*当滚动条的垂直位置大于浏览器所能看到的页面的那部分的高度时，回到顶部按钮就显示 */  
      if ($(window).scrollTop() > $(window).height())  
          backButton.fadeIn();  
      else  
          backButton.fadeOut();  
  });  
  $(window).trigger('scroll');/*触发滚动事件，避免刷新的时候显示回到顶部按钮*/  
  
});