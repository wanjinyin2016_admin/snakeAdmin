<?php

/**
 * @Author: 网名
 * @Date:   2018-06-09 17:27:47
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-10 09:31:53
 */
namespace app\common\model;

use think\Model;

class UserModel extends Model
{
    // 确定链接表名
    protected $name = 'users';


    public function getAccountLogByWhere($param,$where){
        $page = $param['page']?$param['page']:1;
        $limit = $param['limit']?$param['limit']:10; //显示记录数
        $row = db('account_log')->where($where)->limit($page,$limit)->order('log_id desc')->select();
        return $row;
    }

    public function getAccountLogCount($where)
    {  
        $count = db('account_log')->where($where)->count();
        return $count;
    }

}