<?php

/**
 * @Author: Administrator
 * @Date:   2018-05-30 14:43:31
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-07 09:27:04
 */
namespace app\common\logic;

use think\Model;
use think\Page;
use think\Db;

/**
 * 用户逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class UsersLogic extends Model
{


    /*
     * 登陆
     */
    public function login($username,$password)
    {
        if (!$username || !$password) {
            return array('status' => 0, 'msg' => '请填写账号或密码');
        }

        $user = db('users')->where("mobile", $username)->whereOr('email', $username)->find();

        if (!$user) {
            $result = array('status' => -1, 'msg' => '账号不存在!');
        } elseif (encrypt($password) != $user['password']) {
            $result = array('status' => -2, 'msg' => '密码错误!');
        } elseif ($user['is_lock'] == 1) {
            $result = array('status' => -3, 'msg' => '账号异常已被锁定！！！');
        } else {
            $result = array('status' => 1, 'msg' => '登陆成功', 'result' => $user ,'url'=>url('User/index'));
        }
        return $result;
    }


	 /**
     * 注册
     * @param $username  邮箱或手机
     * @param $password  密码
     * @param $password2 确认密码
     * @return array
     */
    public function reg($username,$nickname,$password,$password2){
    	$is_validated = 0 ;
        if(check_mobile($username)){
            $is_validated = 1;
            $map['mobile_validated'] = 1;
            $map['nickname'] = $map['mobile'] = $username; //手机注册
        }
 
        if(!empty($nickname)){
            $map['nickname'] = $nickname;
        }
        
        if(!empty($head_pic)){
            $map['head_pic'] = $head_pic;
        }else{
            $map['head_pic']='/public/images/user_thumb_empty_300.png';
        }

        if($is_validated != 1)
            return array('status'=>-1,'msg'=>'请用手机号','result'=>'');

        if(!$username || !$password)
            return array('status'=>-1,'msg'=>'请输入用户名或密码','result'=>'');

        //验证两次密码是否匹配
        if($password2 != $password)
            return array('status'=>-1,'msg'=>'两次输入密码不一致','result'=>'');
        //验证是否存在用户名

        if(get_user_info($username,2))
            return array('status'=>-1,'msg'=>'账号已存在','result'=>'');

        $map['password'] = encrypt($password);
        $map['reg_time'] = time();
        $map['last_login'] = time();
        $user_level =db('user_level')->where('amount = 0')->find(); //折扣
        $map['discount'] = !empty($user_level) ? $user_level['discount']/100 : 1;  //新注册的会员都不打折
        $user_id = db('users')->insertGetId($map);
        if($user_id === false)
            return array('status'=>-1,'msg'=>'注册失败');
        
        $pay_points = tpCache('basic.reg_integral'); // 会员注册赠送积分
        if($pay_points > 0){
            accountLog($user_id, 0,$pay_points, '会员注册赠送积分'); // 记录日志流水
        }
        $user = db('users')->where("user_id", $user_id)->find();
        return array('status'=>1,'msg'=>'注册成功','result'=>$user);
    }





     /**
     * 检查短信/邮件验证码验证码
     * @param unknown $code
     * @param unknown $sender
     * @param unknown $session_id
     * @return multitype:number string
     */
    public function check_validate_code($code, $sender, $session_id=0 ,$scene = -1){
        
        $timeOut = time();
        $inValid = true;  //验证码失效

        //短信发送否开启
        //-1:用户没有发送短信
        //空:发送验证码关闭
        $sms_status = checkEnableSendSms($scene);

        if($scene == -1){
            return array('status'=>-1,'msg'=>'参数错误, 请传递合理的scene参数');
        }
        
        if(!$code)return array('status'=>-1,'msg'=>'请输入短信验证码');
        //短信
        $sms_time_out = tpCache('sms.sms_time_out');
        $sms_time_out = $sms_time_out ? $sms_time_out : 180;
        $data = db('sms_log')->where(array('mobile'=>$sender,'session_id'=>$session_id , 'status'=>1))->order('id DESC')->find();
        //file_put_contents('./test.log', json_encode(['mobile'=>$sender,'session_id'=>$session_id, 'data' => $data]));
        if(is_array($data) && $data['code'] == $code){
            $data['sender'] = $sender;
            $timeOut = $data['add_time']+ $sms_time_out;
        }else{
            $inValid = false;
        }           
        
       if(empty($data)){
           $res = array('status'=>-1,'msg'=>'请先获取验证码');
       }elseif($timeOut < time()){
           $res = array('status'=>-1,'msg'=>'验证码已超时失效');
       }elseif(!$inValid)
       {
           $res = array('status'=>-1,'msg'=>'验证失败,验证码有误');
       }else{
            $data['is_check'] = 1; //标示验证通过
            session('validate_code',$data);
            $res = array('status'=>1,'msg'=>'验证成功');
        }
        return $res;
    }

    /**
     * 用户充值记录
     * $author lxl 2017-4-26
     * @param $user_id 用户ID
     * @param int $pay_status 充值状态0:待支付 1:充值成功 2:交易关闭
     * @return mixed
     */
    public function get_recharge_log($user_id,$page=1,$limit=10,$pay_status=0){
        $recharge_log_where = ['user_id'=>$user_id];
        if($pay_status){
            $recharge_log_where['pay_status']=$pay_status;
        }
        $count = M('recharge')->where($recharge_log_where)->count();
        $recharge_log = M('recharge')->where($recharge_log_where)
            ->order('order_id desc')
            ->page($page,$limit)
            ->select();
        $return = [
            'code'    =>0,
            'msg'       =>'',
            'data'    =>$recharge_log,
            'count'      =>$count
        ];
        return $return;
    }
}