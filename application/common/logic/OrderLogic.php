<?php

/**
 * @Author: 网名
 * @Date:   2018-06-02 21:47:31
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-02 21:51:27
 */
namespace app\common\logic;

use app\common\logic\wechat\WechatUtil;
use app\common\model\Order;
use app\common\model\SpecGoodsPrice;
use think\Db;

/**
 * Class orderLogic
 * @package Common\Logic
 */
class OrderLogic
{
	protected $user_id=0;
    public function setUserId($user_id){
        $this->user_id=$user_id;
    }

    /**
	 * 自动取消订单
	 */
	public function abolishOrder(){
		$set_time=1; //自动取消时间/天 默认1天
		$abolishtime = strtotime("-$set_time day");
		$order_where = [
				'user_id'      =>$this->user_id,
				'add_time'     =>['lt',$abolishtime],
				'pay_status'   =>0,
				'order_status' => 0
		];
		$order = db('order')->where($order_where)->getTableFields('order_id',true);
		foreach($order as $key =>$value){
			$result = $this->cancel_order($this->user_id,$value);
		}
		return $result;
	}
}