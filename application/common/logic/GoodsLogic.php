<?php

/**
 * @Author: 网名
 * @Date:   2018-06-09 16:31:58
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-09 17:16:33
 */
namespace app\common\logic;

use think\Model;
use think\Page;
use think\Db;

/**
 * 用户逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class GoodsLogic extends Model
{
	public function allAdd($input){
		 $list = array();
		for ($i=0; $i < count($input['goods_name']) ; $i++) { 
            $list[$i]['pack_id'] = $input['pack_id'];
            $list[$i]['goods_name'] = $input['goods_name'][$i];
            $list[$i]['goods_num'] = $input['goods_num'][$i];
            $list[$i]['goods_origin'] = $input['goods_origin'][$i];
            $express = explode('+', $input['express'][$i]);
            $list[$i]['express_code'] = $express[0];
            $list[$i]['express_number'] = $express[1];
            $list[$i]['create_time'] = $input['time'];
        }
        try{
            
		    db('goods')->insertAll($list);
		    return ['status'=>1,'msg'=>'添加成功','url'=>url('Pack/packadd')];
		}catch(\Exception $e){
		    return ['status'=>0,'msg'=>$e->getMessage()];
		}

        
	}
}