<?php

/**
 * @Author: 网名
 * @Date:   2018-06-03 21:01:45
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-03 21:20:27
 */

namespace app\common\logic;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;
/**
 * 二维码逻辑定义
 * Class CatsLogic
 * @package common\Logic
 */
class QrCodeLogic
{
	public function createQrcode($content,$size=300){
		$qrCode = new QrCode($content);
		$qrCode->setSize($size);

		// Set advanced options
		// $qrCode->setWriterByName('png');
		// $qrCode->setMargin(10);
		// $qrCode->setEncoding('UTF-8');
		// $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH);
		// $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
		// $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
		// $qrCode->setLabel('Scan the code', 16, __DIR__.'/../assets/fonts/noto_sans.otf', LabelAlignment::CENTER);
		// $qrCode->setLogoPath(__DIR__.'/../assets/images/symfony.png');
		// $qrCode->setLogoWidth(150);
		// $qrCode->setRoundBlockSize(true);
		// $qrCode->setValidateResult(false);

		// Directly output the QR code
		header('Content-Type: '.$qrCode->getContentType());
		echo $qrCode->writeString();

		// Save it to a file
		// $qrCode->writeFile(__DIR__.'/qrcode.png');

		// Create a response object
		// $response = new QrCodeResponse($qrCode);
	}
}