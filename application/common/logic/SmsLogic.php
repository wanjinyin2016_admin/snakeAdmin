<?php

/**
 * @Author: 网名
 * @Date:   2018-06-01 17:55:56
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-01 20:55:47
 */

namespace app\common\logic;
/**
 * Description of SmsLogic
 *
 * 短信类
 */
class SmsLogic 
{
    private $config;
    
    public function __construct() 
    {
        $this->config = tpCache('sms') ?: [];
    }

    /**
     * 发送短信逻辑
     * @param unknown $scene
     */
    public function sendSms($scene, $sender, $params, $unique_id=0)
    {
        $smsTemp = db('sms_template')->where("send_scene", $scene)->find();    //用户注册.
        $code = !empty($params['code']) ? $params['code'] : false;
        $pack_number = !empty($params['pack_number']) ? $params['pack_number'] : false;
        $express = !empty($params['express']) ? $params['express'] : false;
        
        if(empty($unique_id)){
            $session_id = session_id();
        }else{
            $session_id = $unique_id;
        }
         
        $smsParams = [ // 短信模板中字段的值
            1 => ['code'=>$code],                                                                                                          
            //1. 用户注册 (验证码类型短信只能有一个变量)
            2 => ['code'=>$code],                                                                                                          
            //2. 用户找回密码 (验证码类型短信只能有一个变量)
            3 => ['pack_number'=>$pack_number ,'express'=>$express],                                                      
            //3. 签收包裹
            4 =>['pack_number'=>$pack_number],    
            //4.面单发货                                                                                            
        ];

        $smsParam = $smsParams[$scene];

        //提取发送短信内容
        $scenes = config('SEND_SCENE');
        $msg = $scenes[$scene][1];
        foreach ($smsParam as $k => $v) {
            $msg = str_replace('${' . $k . '}', $v, $msg);
        }

        //发送记录存储数据库
        $log_id = db('sms_log')->insertGetId(array('mobile' => $sender, 'code' => $code, 'add_time' => time(), 'session_id' => $session_id, 'status' => 0, 'scene' => $scene, 'msg' => $msg));
        if ($sender != '' && check_mobile($sender)) {//如果是正常的手机号码才发送
            try {
                $resp = $this->realSendSms($sender, $smsTemp['sms_sign'], $smsParam, $smsTemp['sms_tpl_code']);
            } catch (\Exception $e) {
                $resp = ['status' => -1, 'msg' => $e->getMessage()];
            }
            if ($resp['status'] == 1) {
                db('sms_log')->where(array('id' => $log_id))->update(array('status' => 1)); //修改发送状态为成功
            }else{
                db('sms_log')->where(array('id' => $log_id))->update(array('error_msg'=>$resp['msg'])); //发送失败, 将发送失败信息保存数据库
            }
            return $resp;
        } else {
           return $result = ['status' => -1, 'msg' => '接收手机号不正确['.$sender.']'];
        }
        
    }

    private function realSendSms($mobile, $smsSign, $smsParam, $templateCode)
    {

        $result = $this->sendSmsByAliyun($mobile, $smsSign, $smsParam, $templateCode);
        return $result;
    }
    
   
   
    /**
     * 发送短信（阿里云短信）
     * @param $mobile  手机号码
     * @param $code    验证码
     * @return bool    短信发送成功返回true失败返回false
     */
    private function sendSmsByAliyun($mobile, $smsSign, $smsParam, $templateCode)
    {
        include_once '../vendor/aliyun-php-sdk-core/Config.php';
        include_once '../vendor/Dysmsapi/Request/V20170525/SendSmsRequest.php';
        
        $accessKeyId = $this->config['sms_appkey'];
        $accessKeySecret = $this->config['sms_secretKey'];
        
        //短信API产品名
        $product = "Dysmsapi";
        //短信API产品域名
        $domain = "dysmsapi.aliyuncs.com";
        //暂时不支持多Region
        $region = "cn-hangzhou";

        //初始化访问的acsCleint
        $profile = \DefaultProfile::getProfile($region, $accessKeyId, $accessKeySecret);
        \DefaultProfile::addEndpoint("cn-hangzhou", "cn-hangzhou", $product, $domain);
        $acsClient= new \DefaultAcsClient($profile);

        $request = new \Dysmsapi\Request\V20170525\SendSmsRequest;
        //必填-短信接收号码
        $request->setPhoneNumbers($mobile);
        //必填-短信签名
        $request->setSignName($smsSign);
        //必填-短信模板Code
        $request->setTemplateCode($templateCode);
        // 短信模板中字段的值
        $smsParam = json_encode($smsParam, JSON_UNESCAPED_UNICODE);
        //选填-假如模板中存在变量需要替换则为必填(JSON格式)
        $request->setTemplateParam($smsParam);
        //选填-发送短信流水号
        //$request->setOutId("1234");

        //发起访问请求
        $resp = $acsClient->getAcsResponse($request);
        
        //短信发送成功返回True，失败返回false
        if ($resp && $resp->Code == 'OK') {
            return array('status' => 1, 'msg' => $resp->Code);
        } else {
            return array('status' => -1, 'msg' => $resp->Message . '. Code: ' . $resp->Code);
        }
    }    
}
