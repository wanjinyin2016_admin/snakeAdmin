<?php

/*
*快递鸟物流配置参数
*/

return [
	'RequestData' => 'utf-8', //请求内容需进行URL(utf-8)编码。请求内容JSON格式，须和DataType一致。
	'EBusinessID' => '1282764',//商户ID，请在我的服务页面查看。
	'DataType' => '2', //请求、返回数据类型：2-json；

	'AppKey' =>	'03a58ecb-35c5-40db-b66e-7d42de377a82',
	'ReqURL' =>	'http://api.kdniao.cc/Ebusiness/EbusinessOrderHandle.aspx',
];