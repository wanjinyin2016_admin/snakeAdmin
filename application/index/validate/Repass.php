<?php

/**
 * @Author: 网名
 * @Date:   2018-06-09 17:33:33
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-09 17:35:39
 */
namespace app\index\validate;

use think\Validate;

class Repass extends Validate
{
    protected $rule =   [
        'pass'=>'alphaDash',
        'vercode'=>'require',    
        'username'=>'require',    
        'email'=>'require',    
    ];
    
    protected $message  =   [
        'pass.alphaDash' => '密码不合法',
        'vercode.require'   => '请填写图形验证码',
        'username.require'  => '缺少参数',
        'email.require'  => '缺少参数',
    ];


}