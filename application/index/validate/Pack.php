<?php

/**
 * @Author: 网名
 * @Date:   2018-06-09 17:33:33
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-09 17:35:39
 */
namespace app\index\validate;

use think\Validate;

class Pack extends Validate
{
    protected $rule =   [
        '__token__'  =>  'require|max:32|token',
        'pack_number'=>'require',
        'pack_file'=>'require',
        'pack_note'=>'require',    
    ];
    
    protected $message  =   [
        '__token__.require' => '缺少token',
        '__token__.max' => 'token长度错误',
        '__token__.token' => 'token验证失败',
        'pack_number.require'     => '物流编号不能为空',
        'pack_file.require'   => '请上传面单PDF',
        'pack_note.require'  => '备注信息不能为空',
    ];


}