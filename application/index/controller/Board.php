<?php
namespace app\index\controller;
/**
 * 留言板
 */
class Board extends Base
{
	public function _initialize() 
	{	
		$this->user = array();
		$this->user_id = 0;
         parent::_initialize();
         if(session('?user'))
        {
            $session_user = session('user');
            $user = db('users')->where("user_id", $session_user['user_id'])->find();
            $level = db('user_level')->getField('level_id,level_name');
            $user['level']=$level[$user['level']];
            session('user',$user);  //覆盖session 中的 user     
            $this->user = $user;
            $this->user_id = $user['user_id']; 
        }else{
            $nologin = array('login','register','logout','vertify');
            if(!in_array(ACTION_NAME,$nologin)){
                $this->redirect('Index/User/login');
        		exit;
        	}
        }
        $this->assign('user',$this->user); //存储用户信息
        $this->assign('user_id',$this->user_id);     
    }

	public function index()
	{
		
		return $this->fetch();
	}
}