<?php
namespace app\index\controller;
use app\common\logic\UsersLogic;
use think\Controller;
use think\Verify;

class Main extends Base
{
	public function index(){
		$id = input('id/d');
		$userInfo = get_user_info($id);
		$this->assign([
			'user'=>$userInfo
		]); //存储用户信息
		return $this->fetch();
	}
}