<?php
namespace app\index\controller;

use app\common\logic\QrCodeLogic;
use think\Controller;
use think\Db;

class Index extends Base
{

	public function _initialize() 
    {   
        $this->user = array();
        $this->user_id = 0;
         parent::_initialize();
         if(session('?user'))
        {
            $session_user = session('user');
            $user = db('users')->where("user_id", $session_user['user_id'])->find();
            session('user',$user);  //覆盖session 中的 user     
            $this->user = $user;
            $this->user_id = $user['user_id']; 
        }else{
            $nologin = array('index');
            if(!in_array(ACTION_NAME,$nologin)){
                $this->redirect('Index/User/login');
                exit;
            }
        }
        $this->assign('user',$this->user); //存储用户信息
        $this->assign('user_id',$this->user_id);     
    }

    public function index()
    {	
        return $this->fetch();
    }

    //服务条款
    public function terms()
    {   
        return $this->fetch();
    }

    //二维码生成
    public function qr_code(){
    	$data = input('data');
    	$data = urldecode($data);
    	$QrCodeLogic = new QrCodeLogic();
    	$QrCodeLogic->createQrcode($data);

    }

     /**
     * ajax 修改指定表数据字段  一般修改状态 比如 是否推荐 是否开启 等 图标切换的
     * table,id_name,id_value,field,value
     */
    public function changeTableVal(){  
            $table = input('table/s'); // 表名
            $id_name = input('id_name/s'); // 表主键id名
            $id_value = input('id_value/d'); // 表主键id值
            $field  = input('field/s'); // 修改哪个字段
            $value  = input('value/s'); // 修改字段值                       
            Db::name($table)->where([$id_name => $id_value])->save(array($field=>$value)); // 根据条件保存修改的数据
            $this->ajaxReturn(['status'=>1,'msg'=>'更新成功']);
    }       


    public function activity()
    {
        return $this->fetch();
    }

    public function icon()
    {
        return $this->fetch();
    }
   
}
