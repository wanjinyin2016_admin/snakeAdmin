<?php
/*基类*/
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;

class Base extends Controller {
    /*
     * 初始化操作
     */
    public function _initialize() 
    {
        //访问ip量统计
        $clientIP = getIP();
        $Browser = GetBrowser();
        $row = Db::name('visitor')->where('ip','=',$clientIP)->whereTime('create_time', 'd')->count();
        if ($row==0) { // 当天该IP首次访问网站写入数据库
           $cityInfo = findCityByIp($clientIP)?findCityByIp($clientIP):[];
           $city = isset($cityInfo['data']['city'])?$cityInfo['data']['city']:[];
           Db::name('visitor')->save(['ip'=>$clientIP,'create_time'=>strtotime(date('Y-m-d')),'city'=>$city,'browser'=>$Browser]);
        }
        $this->public_assign();
    }


     /**
     * 保存公告变量到 smarty中 比如 导航 
     */
    public function public_assign()
    {
       $tp_config = array();
       $config = db('config')->cache(true,3600)->select();       
       foreach($config as $k => $v)
       {       
          $tp_config[$v['inc_type'].'_'.$v['name']] = $v['value'];
       }   
       $nav = Db::name('Nav')->where('is_show',1)->select();
       $this->assign('nav',$nav); //存储用户信息
       $this->assign('tp_config', $tp_config);
       $this->assign('action', ACTION_NAME);
       $this->assign('controller', CONTROLLER_NAME);

    }
 
    /*
     * 
     */
    public function ajaxReturn($data)
    {
        exit(json_encode($data));
    }
}