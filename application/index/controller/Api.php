<?php

/**
 * @Author: 网名
 * @Date:   2018-06-01 17:18:28
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-09 15:22:20
 */
namespace app\index\controller;
use app\common\logic\UsersLogic;
use think\Db;
use think\Session;
use think\Verify;
use think\Cookie;
use Kdniao\Kdniao;
/**
* 接口控制器
*/
class Api extends Base
{
	public function _initialize() {
        parent::_initialize();
    }
	
	/**
     * 前端发送短信方法: APP/WAP/PC 共用发送方法
     */
    public function send_validate_code(){
         
        $this->send_scene = config('SEND_SCENE');

        $scene = input('param.scene',1);    //发送短信验证码使用场景
        $mobile = input('param.mobile','');
        $session_id = input('param.unique_id' , session_id());
        session("scene" , $scene);

        if (empty($mobile)) {
        	$this->ajaxReturn(['status'=>0,'msg'=>'手机号码不能为空']);
        }
        
        //发送短信验证码
        $res = checkEnableSendSms($scene);
        if($res['status'] != 1){
            ajaxReturn($res);
        }
        //判断是否存在验证码
        $data = db('sms_log')->where(array('mobile'=>$mobile,'session_id'=>$session_id, 'status'=>1))->order('id DESC')->find();

        //获取时间配置
        $sms_time_out = tpCache('sms.sms_time_out');
        $sms_time_out = $sms_time_out ? $sms_time_out : 120;
        //120秒以内不可重复发送
        if($data && (time() - $data['add_time']) < $sms_time_out){
            $return_arr = array('status'=>-1,'msg'=>$sms_time_out.'秒内不允许重复发送');
            ajaxReturn($return_arr);
        }
        //随机一个验证码
        $code = rand(10000, 99999); 
        $params['code'] =$code;
        //发送短信
        $resp = sendSms($scene , $mobile , $params, $session_id);

        if($resp['status'] == 1){
            //发送成功, 修改发送状态位成功
            db('sms_log')->where(array('mobile'=>$mobile,'code'=>$code,'session_id'=>$session_id , 'status' => 0))->update(array('status' => 1));
            $return_arr = array('status'=>1,'msg'=>'发送成功,请注意查收');
        }else{
            $return_arr = array('status'=>-1,'msg'=>'发送失败'.$resp['msg']);
        }
        $this->ajaxReturn($return_arr);
    }

    //动态检测充值状态
    public function check_recharge_pay_status(){
        $order_id  = input('order_id');
        if(empty($order_id)){
            $res = ['message'=>'参数错误','status'=>-1,'result'=>''];
            $this->AjaxReturn($res);
        }
        $order = M('recharge')->field('pay_status')->where(['order_id'=>$order_id])->find();
        if($order['pay_status'] != 0){
            $res = ['message'=>'已支付','status'=>1,'result'=>$order];
        }else{
            $res = ['message'=>'未支付','status'=>0,'result'=>$order];
        }
        $this->AjaxReturn($res);
    }

    //文件上传
    public function upload(){
        if (IS_POST) {
            // 获取表单上传文件 例如上传了001.jpg
            $file = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            if($file){
                $info = $file->move(ROOT_PATH . 'public' . DS . 'upload/temporary');
                if($info){
                    // 成功上传后 获取上传信息
                   $return = array('status'=>1,'msg'=>'上传成功','result'=>['url'=>'/upload/temporary/'.$info->getSaveName()]);
                }else{
                    // 上传失败获取错误信息
                    $return = array('status'=>1,'msg'=>$file->getError(),'result'=>''); 
                }
                $this->ajaxReturn($return);
            }
        }
         
    }

    //发送验证邮件
    public function activate()
    {
        $email = input('email/s');
        $user = session('user');
        if (empty($user)) {
            $this->ajaxReturn(['status'=>0,'msg'=>'未登录','url'=>url('User/login')]);
        }
        if (empty($email)) {
            $this->ajaxReturn(['status'=>0,'msg'=>'请先设置邮箱','url'=>url('User/set')]);
        }
        $store_name = tpCache('shop_info.store_name');
        $SITE_URL = SITE_URL;
        $subject = '激活邮件';
        $url = SITE_URL.url('User/activate',array('token'=>$user['token']));
        $html = <<<EOF
<div id="mailContentContainer" class="qmbox qm_con_body_content qqmail_webmail_only" style="">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 600px; border: 1px solid #ddd; border-radius: 3px; color: #555; font-family: 'Helvetica Neue Regular',Helvetica,Arial,Tahoma,'Microsoft YaHei','San Francisco','微软雅黑','Hiragino Sans GB',STHeitiSC-Light; font-size: 12px; height: auto; margin: auto; overflow: hidden; text-align: left; word-break: break-all; word-wrap: break-word;">
        <tbody style="margin: 0; padding: 0;">
            <tr style="background-color: #393D49; height: 60px; margin: 0; padding: 0;">
                <td style="margin: 0; padding: 0;">
                    <div style="color: #5EB576; margin: 0; margin-left: 30px; padding: 0;"><a style="font-size: 14px; margin: 0; padding: 0; color: #5EB576; text-decoration: none;" href="$SITE_URL" target="_blank">$store_name</a></div>
                </td>
            </tr>
            <tr style="margin: 0; padding: 0;">
                <td style="margin: 0; padding: 30px;">
                    <p style="line-height: 20px; margin: 0; margin-bottom: 10px; padding: 0;"> Hi，<em style="font-weight: 700;">{$user['nickname']}</em>，请完成以下操作： </p>
                    <div style=""> <a href="$url" style="background-color: #009E94; color: #fff; display: inline-block; height: 32px; line-height: 32px; margin: 0 15px 0 0; padding: 0 15px; text-decoration: none;" target="_blank">立即激活邮箱</a> </div>
                    <p style="line-height: 20px; margin-top: 20px; padding: 10px; background-color: #f2f2f2; font-size: 12px;"> 如果该邮件不是由你本人操作，请勿进行激活！否则你的邮箱将会被他人绑定。 </p>
                </td>
            </tr>
            <tr style="background-color: #fafafa; color: #999; height: 35px; margin: 0; padding: 0; text-align: center;">
                <td style="margin: 0; padding: 0;">系统邮件，请勿直接回复。</td>
            </tr>
        </tbody>
    </table>
    <style type="text/css">
    .qmbox style,
    .qmbox script,
    .qmbox head,
    .qmbox link,
    .qmbox meta {
        display: none !important;
    }
    </style>
</div>
EOF;
        $return = send_email($email, $subject, $html);
        $this->ajaxReturn($return);
    }


    //检测物流
    public function checkShipping(){
        $ShipperCode = input('type/s');
        $LogisticCode = input('code/s');
        $Kdniao = new Kdniao();
        //识别单号 
        $obj = $Kdniao->getLogisticsTrack($LogisticCode,$ShipperCode);
        // $result = object_to_array(json_decode($obj,1))
        exit($obj);
    }

    //通用图形码校验
    public function checkVercode()
    {
        $verify = new Verify();
        $vercode = input('vercode/s','');
        $type = input('type/s','user_login'); //默认用户登陆校验
        if (!$verify->check($vercode,$type))
        {
            $this->ajaxReturn(['status'=>0,'msg'=>'图形码错误']);
        }else{
            $this->ajaxReturn(['status'=>1,'msg'=>'图形码正确']);
        }
    }


   
}