<?php
namespace app\talk\model;

use think\Model;

class Article extends Model
{
	// 设置当前模型对应的完整数据表名称
    protected $table = 'snake_articles';


    public function comment()
    {
        return $this->hasMany('Comment','article_id');
    }

    public function zan()
    {
        return $this->hasMany('Zan','type_id');
    }
}