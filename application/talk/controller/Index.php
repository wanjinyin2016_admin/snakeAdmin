<?php
namespace app\talk\controller;
use think\Db;
use think\Request;
class Index extends Base
{
	public function _initialize() 
    {
    	$actionName = $this->request->action();
    	if (!in_array($actionName, array('index','message','about'))) {
    		$actionName = 'index';
    	}
    	$this->assign('actionName',$actionName);
    }

    public function index()
    {	
    	$page = input('page/d',1);
    	$list = model('Article')->page($page,3)->order('id desc')->select();
    	$this->assign('list',$list);
        return $this->fetch();
    }

	public function message()
    {
        return $this->fetch();
    }

    public function details()
    {	
    	$id = input('id/d',0);
    	$info = model('Article')->with('Comment')->where(['id'=>$id])->find();
    	if (empty($info)) {
    		$this->error('文章不存在');
    	}
    	$info['add_time'] = friend_date(strtotime($info->add_time));
    	$info['zanCount'] = model('Zan')->where(['type_id'=>$info->id])->count();
    	$this->assign('info',$info);
        return $this->fetch();
    }

    public function comment()
    {
        $id = input('id/d',0);
        $info = model('Article')->where(['id'=>$id])->find();
        if (empty($info)) {
            $this->error('文章不存在');
        }
        $info['add_time'] = friend_date(strtotime($info->add_time));
        $info['zanCount'] = model('Zan')->where(['type_id'=>$info->id])->count();
        $this->assign('info',$info);
        return $this->fetch();
    }

    public function about()
    {
        return $this->fetch();
    }
}
