/*
Navicat MySQL Data Transfer

Source Server         : 120.78.138.24
Source Server Version : 50557
Source Host           : 120.78.138.24:3306
Source Database       : snakeAdmin

Target Server Type    : MYSQL
Target Server Version : 50557
File Encoding         : 65001

Date: 2018-06-30 10:37:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for snake_account_log
-- ----------------------------
DROP TABLE IF EXISTS `snake_account_log`;
CREATE TABLE `snake_account_log` (
  `log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `user_money` decimal(10,2) DEFAULT '0.00' COMMENT '用户金额',
  `frozen_money` decimal(10,2) DEFAULT '0.00' COMMENT '冻结金额',
  `pay_points` mediumint(9) NOT NULL DEFAULT '0' COMMENT '支付积分',
  `change_time` int(10) unsigned NOT NULL COMMENT '变动时间',
  `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `order_sn` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `order_id` int(10) DEFAULT NULL COMMENT '订单id',
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_account_log
-- ----------------------------
INSERT INTO `snake_account_log` VALUES ('1', '2876', '0.00', '0.00', '100', '1527862310', '会员注册赠送积分', '', '0');
INSERT INTO `snake_account_log` VALUES ('2', '2877', '0.00', '0.00', '100', '1527862785', '会员注册赠送积分', '', '0');
INSERT INTO `snake_account_log` VALUES ('3', '2878', '0.00', '0.00', '100', '1527863145', '会员注册赠送积分', '', '0');
INSERT INTO `snake_account_log` VALUES ('4', '2879', '0.00', '0.00', '100', '1528003837', '会员注册赠送积分', '', '0');
INSERT INTO `snake_account_log` VALUES ('5', '2878', '0.01', '0.00', '0', '1528039210', '会员在线充值', 'recharger9H3okcSdQ', '0');
INSERT INTO `snake_account_log` VALUES ('6', '2878', '0.01', '0.00', '0', '1528039322', '会员在线充值', 'rechargeFzbQ8Mqcel', '0');
INSERT INTO `snake_account_log` VALUES ('7', '2878', '0.01', '0.00', '0', '1528040535', '会员在线充值', 'rechargeEGfEH9Dqb1', '0');
INSERT INTO `snake_account_log` VALUES ('8', '2878', '0.01', '0.00', '0', '1528294978', '会员在线充值', 'rechargeq8ttSizMCr', '0');
INSERT INTO `snake_account_log` VALUES ('9', '2880', '0.00', '0.00', '100', '1528620317', '会员注册赠送积分', '', '0');
INSERT INTO `snake_account_log` VALUES ('10', '2878', '10.00', '0.00', '0', '1529844155', '系统添加10元', '', '0');
INSERT INTO `snake_account_log` VALUES ('11', '2878', '10.00', '0.00', '0', '1529844167', '系统添加10元', '', '0');
INSERT INTO `snake_account_log` VALUES ('12', '2878', '10.00', '0.00', '0', '1529844170', '系统添加10元', '', '0');
INSERT INTO `snake_account_log` VALUES ('13', '2878', '10.00', '0.00', '0', '1529844581', '扣除', '', '0');
INSERT INTO `snake_account_log` VALUES ('14', '2878', '10.00', '0.00', '0', '1529844676', '扣除', '', '0');
INSERT INTO `snake_account_log` VALUES ('15', '2878', '40.00', '0.00', '0', '1529844800', '扣除40', '', '0');
INSERT INTO `snake_account_log` VALUES ('16', '2878', '-50.00', '0.00', '0', '1529844863', '扣除50', '', '0');
INSERT INTO `snake_account_log` VALUES ('17', '2880', '50.00', '0.00', '0', '1530275607', '会员在线充值', 'rechargevfDGsv8mDq', '0');
INSERT INTO `snake_account_log` VALUES ('18', '2878', '-3.00', '0.00', '0', '1530285665', '代发下单', '', '0');
INSERT INTO `snake_account_log` VALUES ('19', '2878', '-3.00', '0.00', '0', '1530286159', '代发下单', '', '0');
INSERT INTO `snake_account_log` VALUES ('20', '2878', '-3.00', '0.00', '0', '1530286247', '代发下单', '', '0');

-- ----------------------------
-- Table structure for snake_articles
-- ----------------------------
DROP TABLE IF EXISTS `snake_articles`;
CREATE TABLE `snake_articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(155) NOT NULL COMMENT '文章标题',
  `description` varchar(255) NOT NULL COMMENT '文章描述',
  `keywords` varchar(155) NOT NULL COMMENT '文章关键字',
  `thumbnail` varchar(255) NOT NULL COMMENT '文章缩略图',
  `content` text NOT NULL COMMENT '文章内容',
  `add_time` datetime NOT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_articles
-- ----------------------------
INSERT INTO `snake_articles` VALUES ('2', '文章标题', '文章描述', '关键字1,关键字2,关键字3', '/upload/20170916/1e915c70dbb9d3e8a07bede7b64e4cff.png', '<p><img src=\"/upload/image/20170916/1505555254.png\" title=\"1505555254.png\" alt=\"QQ截图20170916174651.png\"/></p><p>测试文章内容</p><p>测试内容</p>', '2017-09-16 17:47:44');

-- ----------------------------
-- Table structure for snake_config
-- ----------------------------
DROP TABLE IF EXISTS `snake_config`;
CREATE TABLE `snake_config` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `name` varchar(50) DEFAULT NULL COMMENT '配置的key键名',
  `value` varchar(512) DEFAULT NULL COMMENT '配置的val值',
  `inc_type` varchar(64) DEFAULT NULL COMMENT '配置分组',
  `desc` varchar(50) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_config
-- ----------------------------
INSERT INTO `snake_config` VALUES ('124', 'reg_integral', '100', 'basic', '注册送积分');
INSERT INTO `snake_config` VALUES ('125', 'regis_sms_enable', '1', 'sms', '开启注册短信验证');
INSERT INTO `snake_config` VALUES ('126', 'sms_time_out', '600', 'sms', '验证码过期时间');
INSERT INTO `snake_config` VALUES ('127', 'sms_appkey', 'LTAIWVPi3BO0yqsv', 'sms', null);
INSERT INTO `snake_config` VALUES ('128', 'sms_secretKey', 'RMXQbSUmU3olwKik1fLgluGkSsSe6G', 'sms', null);
INSERT INTO `snake_config` VALUES ('129', 'sms_product', '电商之家', 'sms', null);
INSERT INTO `snake_config` VALUES ('130', 'record_no', '粤ICP备16028635号-2', 'shop_info', null);
INSERT INTO `snake_config` VALUES ('131', 'store_name', '电商之家', 'shop_info', null);
INSERT INTO `snake_config` VALUES ('132', 'store_keyword', '电商服务', 'shop_info', null);
INSERT INTO `snake_config` VALUES ('133', 'contact', '王总', 'shop_info', null);
INSERT INTO `snake_config` VALUES ('134', 'phone', '15361698313', 'shop_info', null);
INSERT INTO `snake_config` VALUES ('135', 'address', '深圳市 龙岗区', 'shop_info', null);
INSERT INTO `snake_config` VALUES ('136', 'qq', '1437935165', 'shop_info', null);
INSERT INTO `snake_config` VALUES ('137', 'smtp_server', 'smtp.mxhichina.com', 'smtp', null);
INSERT INTO `snake_config` VALUES ('138', 'smtp_port', '465', 'smtp', null);
INSERT INTO `snake_config` VALUES ('139', 'smtp_user', 'service@88tu.cn', 'smtp', null);
INSERT INTO `snake_config` VALUES ('140', 'smtp_pwd', 'Yjseo33567652', 'smtp', null);
INSERT INTO `snake_config` VALUES ('141', 'test_eamil', '1437935165@qq.com', 'smtp', null);
INSERT INTO `snake_config` VALUES ('142', 'free', '3', 'shop_info', null);

-- ----------------------------
-- Table structure for snake_goods_2018
-- ----------------------------
DROP TABLE IF EXISTS `snake_goods_2018`;
CREATE TABLE `snake_goods_2018` (
  `goods_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pack_id` int(11) NOT NULL COMMENT '面单id',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_num` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `goods_origin` varchar(255) NOT NULL COMMENT '商品图片地址',
  `express_code` varchar(50) NOT NULL COMMENT '快递公司编码',
  `express_number` varchar(150) NOT NULL COMMENT '快递单号',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0待确认 1确认 2商品丢失',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_goods_2018
-- ----------------------------
INSERT INTO `snake_goods_2018` VALUES ('1', '2', '电脑主板', '2', 'https://88tu.cn//public/upload/goods/thumb/424/goods_thumb_424_236_236.jpeg', 'SF', '5465498791', '1529895858', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('2', '2', '电脑显示器', '1', 'https://88tu.cn//public/upload/goods/thumb/424/goods_thumb_424_236_236.jpeg', 'SF', '5465498791', '1529895858', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('4', '3', '水电费', '5', 'http://yiqubao.oss-cn-shenzhen.aliyuncs.com/public/upload/goods/2018/03-17/57a0e9c7cb619adff8afd104e17f30e1.png?x-oss-process=image/resize,m_pad,h_400,w_400', 'SF', '566456546', '1530266423', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('5', '10', '鞋子', '2', 'https://timgsa.baidu.com/timg?image&amp;quality=80&amp;size=b9999_10000&amp;sec=1530289223049&amp;di=025e4e3d34e2f669c94b42a8ccbb6cbe&amp;imgtype=0&amp;src=http%3A%2F%2Fwww.sinokin.com%2Fimgit%2FP4ebcd6eb732f5.gif', 'SF', '65465468498854', '1530279172', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('6', '13', '啊啊啊', '1', 'https://timgsa.baidu.com/timg?image&amp;quality=80&amp;size=b9999_10000&amp;sec=1530289223049&amp;di=70a4659a8a389684de9889a2414bba97&amp;imgtype=0&amp;src=http%3A%2F%2Fwww.sinokin.com%2Fimgit%2FP4eb8dfdaca7d5.gif', 'SF', '', '1530279387', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('7', '16', '啊啊啊', '12', 'https://timgsa.baidu.com/timg?image&amp;quality=80&amp;size=b9999_10000&amp;sec=1530289223047&amp;di=ac6ede71d754a54393c06fef3bd8db3a&amp;imgtype=0&amp;src=http%3A%2F%2F58pic.ooopic.com%2F58pic%2F16%2F11%2F25%2F36s58PICxfT.jpg', 'SF', '5465546546', '1530279590', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('8', '17', '巧克力', '1', 'https://timgsa.baidu.com/timg?image&amp;quality=80&amp;size=b9999_10000&amp;sec=1530289223044&amp;di=4a4fcaa3c125b44edefabe99e2daa0e0&amp;imgtype=0&amp;src=http%3A%2F%2Fwww.it365.net%2Fimgit%2FP4ea7c2918f6e2.gif', 'SF', '64656', '1530279856', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('9', '24', '鞋子', '2', 'https://timgsa.baidu.com/timg?image&amp;quality=80&amp;size=b9999_10000&amp;sec=1530289223043&amp;di=6f8dfb977403cabc07b992b2ca4968d5&amp;imgtype=0&amp;src=http%3A%2F%2Fimgqn.koudaitong.com%2Fupload_files%2F2015%2F04%2F04%2FFsMiAopH-AU-QDQI5zA7L6dtpmct.jp', 'SF', '6546549879', '1530281327', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('10', '25', '鞋子', '2', 'https://timgsa.baidu.com/timg?image&amp;quality=80&amp;size=b9999_10000&amp;sec=1530289223043&amp;di=6f8dfb977403cabc07b992b2ca4968d5&amp;imgtype=0&amp;src=http%3A%2F%2Fimgqn.koudaitong.com%2Fupload_files%2F2015%2F04%2F04%2FFsMiAopH-AU-QDQI5zA7L6dtpmct.jp', 'SF', '6546549879', '1530281376', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('11', '26', '2018春夏季薄款棉绸女童背心裙空调房宝宝家居睡裙绵绸无袖连衣裙', '2', 'https://gd2.alicdn.com/imgextra/i4/80212702/TB28zh8gr1YBuNjSszhXXcUsFXa_!!80212702.jpg_400x400.jpg', 'SF', '654654654', '1530285479', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('12', '27', '2018春夏季薄款棉绸女童背心裙空调房宝宝家居睡裙绵绸无袖连衣裙', '2', 'https://gd2.alicdn.com/imgextra/i4/80212702/TB28zh8gr1YBuNjSszhXXcUsFXa_!!80212702.jpg_400x400.jpg', 'SF', '654654654', '1530285643', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('13', '28', '2018春夏季薄款棉绸女童背心裙空调房宝宝家居睡裙绵绸无袖连衣裙', '2', 'https://gd2.alicdn.com/imgextra/i4/80212702/TB28zh8gr1YBuNjSszhXXcUsFXa_!!80212702.jpg_400x400.jpg', 'SF', '654654654', '1530285665', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('14', '29', '女童旗袍2018新款夏季宝宝唐装儿童旗袍连衣裙汉服中国风童装春秋', '2', 'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/1053247021/TB2dJFWdlfM8KJjSZPiXXXdspXa_!!1053247021.jpg_250x250.jpg_.webp', 'SF', '654654654', '1530286159', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('15', '29', '童装2018新款儿童春秋装女孩连衣裙夏季宝宝吊带裙女童牛仔背带裙', '2', 'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/2775860318/TB1w4ZeXXTM8KJjSZFlXXaO8FXa_!!0-item_pic.jpg_250x250.jpg_.webp', 'SF', '654654654', '1530286159', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('17', '30', '女童旗袍2018新款夏季宝宝唐装儿童旗袍连衣裙汉服中国风童装春秋', '2', 'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/1053247021/TB2dJFWdlfM8KJjSZPiXXXdspXa_!!1053247021.jpg_250x250.jpg_.webp', 'SF', '654654654', '1530286217', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('18', '30', '童装2018新款儿童春秋装女孩连衣裙夏季宝宝吊带裙女童牛仔背带裙', '2', 'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/2775860318/TB1w4ZeXXTM8KJjSZFlXXaO8FXa_!!0-item_pic.jpg_250x250.jpg_.webp', 'SF', '654654654', '1530286217', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('20', '31', '女童旗袍2018新款夏季宝宝唐装儿童旗袍连衣裙汉服中国风童装春秋', '2', 'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/1053247021/TB2dJFWdlfM8KJjSZPiXXXdspXa_!!1053247021.jpg_250x250.jpg_.webp', 'SF', '654654654', '1530286247', '0', null);
INSERT INTO `snake_goods_2018` VALUES ('21', '31', '童装2018新款儿童春秋装女孩连衣裙夏季宝宝吊带裙女童牛仔背带裙', '2', 'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/2775860318/TB1w4ZeXXTM8KJjSZFlXXaO8FXa_!!0-item_pic.jpg_250x250.jpg_.webp', 'SF', '654654654', '1530286247', '0', null);

-- ----------------------------
-- Table structure for snake_goods_2019
-- ----------------------------
DROP TABLE IF EXISTS `snake_goods_2019`;
CREATE TABLE `snake_goods_2019` (
  `goods_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pack_id` int(11) NOT NULL COMMENT '面单id',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_num` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `goods_origin` varchar(255) NOT NULL COMMENT '商品图片地址',
  `express_code` varchar(50) NOT NULL COMMENT '快递公司编码',
  `express_number` varchar(150) NOT NULL COMMENT '快递单号',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_goods_2019
-- ----------------------------

-- ----------------------------
-- Table structure for snake_goods_2020
-- ----------------------------
DROP TABLE IF EXISTS `snake_goods_2020`;
CREATE TABLE `snake_goods_2020` (
  `goods_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pack_id` int(11) NOT NULL COMMENT '面单id',
  `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_num` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `goods_origin` varchar(255) NOT NULL COMMENT '商品图片地址',
  `express_code` varchar(50) NOT NULL COMMENT '快递公司编码',
  `express_number` varchar(150) NOT NULL COMMENT '快递单号',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_goods_2020
-- ----------------------------

-- ----------------------------
-- Table structure for snake_node
-- ----------------------------
DROP TABLE IF EXISTS `snake_node`;
CREATE TABLE `snake_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_name` varchar(155) NOT NULL DEFAULT '' COMMENT '节点名称',
  `control_name` varchar(155) NOT NULL DEFAULT '' COMMENT '控制器名',
  `action_name` varchar(155) NOT NULL COMMENT '方法名',
  `is_menu` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否是菜单项 1不是 2是',
  `type_id` int(11) NOT NULL COMMENT '父级节点id',
  `style` varchar(155) DEFAULT '' COMMENT '菜单样式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of snake_node
-- ----------------------------
INSERT INTO `snake_node` VALUES ('1', '权限管理', '#', '#', '2', '0', 'fa fa-users');
INSERT INTO `snake_node` VALUES ('2', '管理员管理', 'user', 'index', '2', '1', '');
INSERT INTO `snake_node` VALUES ('3', '添加管理员', 'user', 'useradd', '1', '2', '');
INSERT INTO `snake_node` VALUES ('4', '编辑管理员', 'user', 'useredit', '1', '2', '');
INSERT INTO `snake_node` VALUES ('5', '删除管理员', 'user', 'userdel', '1', '2', '');
INSERT INTO `snake_node` VALUES ('6', '角色管理', 'role', 'index', '2', '1', '');
INSERT INTO `snake_node` VALUES ('7', '添加角色', 'role', 'roleadd', '1', '6', '');
INSERT INTO `snake_node` VALUES ('8', '编辑角色', 'role', 'roleedit', '1', '6', '');
INSERT INTO `snake_node` VALUES ('9', '删除角色', 'role', 'roledel', '1', '6', '');
INSERT INTO `snake_node` VALUES ('10', '分配权限', 'role', 'giveaccess', '1', '6', '');
INSERT INTO `snake_node` VALUES ('11', '系统管理', '#', '#', '2', '0', 'fa fa-desktop');
INSERT INTO `snake_node` VALUES ('12', '数据备份/还原', 'data', 'index', '2', '11', '');
INSERT INTO `snake_node` VALUES ('13', '备份数据', 'data', 'importdata', '1', '12', '');
INSERT INTO `snake_node` VALUES ('14', '还原数据', 'data', 'backdata', '1', '12', '');
INSERT INTO `snake_node` VALUES ('15', '节点管理', 'node', 'index', '2', '1', '');
INSERT INTO `snake_node` VALUES ('16', '添加节点', 'node', 'nodeadd', '1', '15', '');
INSERT INTO `snake_node` VALUES ('17', '编辑节点', 'node', 'nodeedit', '1', '15', '');
INSERT INTO `snake_node` VALUES ('18', '删除节点', 'node', 'nodedel', '1', '15', '');
INSERT INTO `snake_node` VALUES ('19', '文章管理', 'articles', 'index', '2', '0', 'fa fa-book');
INSERT INTO `snake_node` VALUES ('20', '文章列表', 'articles', 'index', '2', '19', '');
INSERT INTO `snake_node` VALUES ('21', '添加文章', 'articles', 'articleadd', '1', '19', '');
INSERT INTO `snake_node` VALUES ('22', '编辑文章', 'articles', 'articleedit', '1', '19', '');
INSERT INTO `snake_node` VALUES ('23', '删除文章', 'articles', 'articledel', '1', '19', '');
INSERT INTO `snake_node` VALUES ('24', '上传图片', 'articles', 'uploadImg', '1', '19', '');
INSERT INTO `snake_node` VALUES ('25', '个人中心', '#', '#', '1', '0', '');
INSERT INTO `snake_node` VALUES ('26', '编辑信息', 'profile', 'index', '1', '25', '');
INSERT INTO `snake_node` VALUES ('27', '编辑头像', 'profile', 'headedit', '1', '25', '');
INSERT INTO `snake_node` VALUES ('28', '上传头像', 'profile', 'uploadheade', '1', '25', '');
INSERT INTO `snake_node` VALUES ('29', '用户管理', '#', '#', '2', '0', 'fa fa-cog');
INSERT INTO `snake_node` VALUES ('30', '用户列表', 'users', 'index', '2', '29', 'fa fa-cog');
INSERT INTO `snake_node` VALUES ('31', '添加用户', 'users', 'useradd', '1', '29', '');
INSERT INTO `snake_node` VALUES ('33', '编辑用户', 'users', 'useredit', '1', '29', '');
INSERT INTO `snake_node` VALUES ('34', '删除用户', 'users', 'userdel', '1', '29', '');
INSERT INTO `snake_node` VALUES ('35', '等级管理', 'user_level', 'index', '2', '0', 'fa fa-cog');
INSERT INTO `snake_node` VALUES ('36', '用户等级', 'user_level', 'index', '2', '35', '');
INSERT INTO `snake_node` VALUES ('37', '添加等级', 'user_level', 'level_add', '1', '35', '');
INSERT INTO `snake_node` VALUES ('38', '修改等级', 'user_level', 'leve_ledit', '1', '35', '');
INSERT INTO `snake_node` VALUES ('39', '短信模版', 'sms_template', 'index', '2', '11', '');
INSERT INTO `snake_node` VALUES ('40', '添加模版', 'sms_template', 'add_sms_template', '1', '39', '');
INSERT INTO `snake_node` VALUES ('42', '编辑模版', 'sms_template', 'edit_sms_template', '1', '39', '');
INSERT INTO `snake_node` VALUES ('43', '删除模版', 'sms_template', 'del_template', '1', '39', '');
INSERT INTO `snake_node` VALUES ('44', '系统设置', 'system', 'index', '2', '11', '');
INSERT INTO `snake_node` VALUES ('45', '清理缓存', 'system', 'cleancache', '1', '44', '');
INSERT INTO `snake_node` VALUES ('46', '插件管理', 'plugin', 'index', '2', '11', '');
INSERT INTO `snake_node` VALUES ('47', '一键安装', 'plugin', 'install', '1', '46', '');
INSERT INTO `snake_node` VALUES ('48', '卸载', 'plugin', 'unstall', '1', '46', '');
INSERT INTO `snake_node` VALUES ('49', '配置', 'plugin', 'setting', '1', '46', '');
INSERT INTO `snake_node` VALUES ('50', '资金明细', 'users', 'accountlog', '1', '29', '');
INSERT INTO `snake_node` VALUES ('51', '资金调节', 'users', 'accountlogupdate', '1', '29', '');
INSERT INTO `snake_node` VALUES ('52', '面单管理', '#', '#', '2', '0', 'fa fa-barcode');
INSERT INTO `snake_node` VALUES ('53', '面单列表', 'pack', 'index', '2', '52', '');
INSERT INTO `snake_node` VALUES ('54', '打印面单', 'pack', 'print', '1', '52', '');

-- ----------------------------
-- Table structure for snake_oauth_users
-- ----------------------------
DROP TABLE IF EXISTS `snake_oauth_users`;
CREATE TABLE `snake_oauth_users` (
  `tu_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '表自增ID',
  `user_id` mediumint(8) NOT NULL COMMENT '用户表ID',
  `openid` varchar(255) NOT NULL COMMENT '第三方开放平台openid',
  `oauth` varchar(50) NOT NULL COMMENT '第三方授权平台',
  `unionid` varchar(255) DEFAULT NULL COMMENT 'unionid',
  `oauth_child` varchar(50) DEFAULT NULL COMMENT 'mp标识来自公众号, open标识来自开放平台,用于标识来自哪个第三方授权平台, 因为同是微信平台有来自公众号和开放平台',
  PRIMARY KEY (`tu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_oauth_users
-- ----------------------------

-- ----------------------------
-- Table structure for snake_pack_2018
-- ----------------------------
DROP TABLE IF EXISTS `snake_pack_2018`;
CREATE TABLE `snake_pack_2018` (
  `pack_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `pack_number` varchar(150) NOT NULL COMMENT '追踪号码',
  `pack_file` varchar(255) NOT NULL COMMENT 'pdf文件地址',
  `pack_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '面单状态 0未提交 1已提交 2已确认 3已签收 4已扫描 5已完成',
  `pack_note` varchar(255) NOT NULL COMMENT '客户备注信息',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `pay_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未支付 1已支付',
  `free` decimal(10,0) NOT NULL DEFAULT '0' COMMENT '费用',
  PRIMARY KEY (`pack_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_pack_2018
-- ----------------------------
INSERT INTO `snake_pack_2018` VALUES ('1', '2878', '65465465465', '/upload/pdf/20180625/d59555596e535b87ed597f391c9f76e4.png', '0', '24234234', '1529895558', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('2', '2878', '20180625001', '/upload/pdf/20180625/9098d32d8fe8e707741f2e9b04789943.png', '0', '一个免单两个产品 主板+显示器', '1529895858', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('3', '2878', '8549849849', '/upload/temporary/20180629/bea63aba025da780ddc70acd1e5a249e.jpg', '0', '是的发送到发送到', '1530266423', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('4', '2880', 'LZDCB00123467466', '', '0', '黑色。M', '1530277071', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('5', '2880', 'LZDCB00123467466', '', '0', '黑色。M', '1530277089', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('6', '2880', 'LZDCB00123467466', '', '0', '黑色。M', '1530277091', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('7', '2880', 'LZDCB00123467466', '', '0', '黑色。M', '1530277095', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('8', '2880', 'LZDCB00123467466', '', '0', '黑色。M', '1530277098', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('9', '2880', 'LZDCB00123467466', '', '0', '黑色。M', '1530277107', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('10', '2878', 'OP00442015015830', '', '0', '注意颜色', '1530279172', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('11', '2880', 'LZDCB00122375351', '', '0', '黑色M', '1530279363', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('12', '2880', 'LZDCB00122375351', '', '0', '黑色M', '1530279368', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('13', '2878', 'OP00442015015830', '', '0', '饮料', '1530279387', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('14', '2880', 'LZDCB00122375351', '', '0', '黑色M', '1530279418', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('15', '2880', 'LZDCB00122375351', '', '0', '黑色M', '1530279424', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('16', '2878', 'OP00442015015830', '', '0', '巧克力', '1530279590', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('17', '2878', 'OP00442015015830', '/upload/temporary/20180629/7a865d59e1c4c0b0ac1a209169512826.pdf', '0', '巧克力', '1530279856', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('18', '2880', 'LZDCB00122375351', '', '0', '黑色M', '1530280637', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('19', '2880', 'LZDCB00122375351', '/upload/temporary/20180629/c7b69686321463857d62f9b9c2c2f79f.pdf', '0', '', '1530280744', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('20', '2880', 'LZDCB00122375351', '/upload/temporary/20180629/c7b69686321463857d62f9b9c2c2f79f.pdf', '0', '', '1530280752', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('21', '2880', 'LZDCB000122375351', '/upload/temporary/20180629/e84884139882ef6eee334d867697d7ae.pdf', '0', '黑色M', '1530281007', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('22', '2878', 'OP00442015015830', '/upload/temporary/20180629/50f738f8f6d4d3f43b5aae747d81d813.pdf', '0', '鞋子', '1530281203', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('23', '2878', 'OP00442015015830', '/upload/temporary/20180629/50f738f8f6d4d3f43b5aae747d81d813.pdf', '0', '鞋子', '1530281209', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('24', '2878', 'OP00442015015830', '/upload/temporary/20180629/50f738f8f6d4d3f43b5aae747d81d813.pdf', '0', '鞋子', '1530281327', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('25', '2878', 'OP00442015015830', '/upload/temporary/20180629/50f738f8f6d4d3f43b5aae747d81d813.pdf', '0', '鞋子', '1530281376', '0', '0', '0');
INSERT INTO `snake_pack_2018` VALUES ('26', '2878', 'LZDCB00099543475', '/upload/pdf/20180629/7482f98aa67c8b48ea3ff01a0cfc84a7.pdf', '0', '2018春夏季薄款棉绸女童背心裙空调房宝宝家居睡裙绵绸无袖连衣裙', '1530285479', '0', '0', '3');
INSERT INTO `snake_pack_2018` VALUES ('27', '2878', 'LZDCB00099543475', '/upload/pdf/20180629/7482f98aa67c8b48ea3ff01a0cfc84a7.pdf', '0', '2018春夏季薄款棉绸女童背心裙空调房宝宝家居睡裙绵绸无袖连衣裙', '1530285643', '0', '0', '3');
INSERT INTO `snake_pack_2018` VALUES ('28', '2878', 'LZDCB00099543475', '/upload/pdf/20180629/7482f98aa67c8b48ea3ff01a0cfc84a7.pdf', '0', '2018春夏季薄款棉绸女童背心裙空调房宝宝家居睡裙绵绸无袖连衣裙', '1530285665', '0', '1', '3');
INSERT INTO `snake_pack_2018` VALUES ('29', '2878', 'LZDCB00099543475', '/upload/pdf/20180629/7a508ffa3a7114296e05d44c675aa41c.pdf', '0', '童装2018新款儿童春秋装女孩连衣裙夏季宝宝吊带裙女童牛仔背带裙', '1530286159', '0', '1', '3');
INSERT INTO `snake_pack_2018` VALUES ('30', '2878', 'LZDCB00099543475', '/upload/pdf/20180629/7a508ffa3a7114296e05d44c675aa41c.pdf', '0', '童装2018新款儿童春秋装女孩连衣裙夏季宝宝吊带裙女童牛仔背带裙', '1530286217', '0', '0', '3');
INSERT INTO `snake_pack_2018` VALUES ('31', '2878', 'LZDCB00099543475', '/upload/pdf/20180629/7a508ffa3a7114296e05d44c675aa41c.pdf', '0', '童装2018新款儿童春秋装女孩连衣裙夏季宝宝吊带裙女童牛仔背带裙', '1530286247', '0', '1', '3');

-- ----------------------------
-- Table structure for snake_pack_2019
-- ----------------------------
DROP TABLE IF EXISTS `snake_pack_2019`;
CREATE TABLE `snake_pack_2019` (
  `pack_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `pack_number` varchar(150) NOT NULL COMMENT '追踪号码',
  `pack_file` varchar(255) NOT NULL COMMENT 'pdf文件地址',
  `pack_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '面单状态 0未提交 1已提交 2已确认 3已签收 4已扫描 5已完成',
  `pack_note` varchar(255) NOT NULL COMMENT '客户备注信息',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`pack_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_pack_2019
-- ----------------------------

-- ----------------------------
-- Table structure for snake_pack_2020
-- ----------------------------
DROP TABLE IF EXISTS `snake_pack_2020`;
CREATE TABLE `snake_pack_2020` (
  `pack_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `pack_number` varchar(150) NOT NULL COMMENT '追踪号码',
  `pack_file` varchar(255) NOT NULL COMMENT 'pdf文件地址',
  `pack_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '面单状态 0未提交 1已提交 2已确认 3已签收 4已扫描 5已完成',
  `pack_note` varchar(255) NOT NULL COMMENT '客户备注信息',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`pack_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_pack_2020
-- ----------------------------

-- ----------------------------
-- Table structure for snake_plugin
-- ----------------------------
DROP TABLE IF EXISTS `snake_plugin`;
CREATE TABLE `snake_plugin` (
  `code` varchar(13) DEFAULT NULL COMMENT '插件编码',
  `name` varchar(55) DEFAULT NULL COMMENT '中文名字',
  `version` varchar(255) DEFAULT NULL COMMENT '插件的版本',
  `author` varchar(30) DEFAULT NULL COMMENT '插件作者',
  `config` text COMMENT '配置信息',
  `config_value` text COMMENT '配置值信息',
  `desc` varchar(255) DEFAULT NULL COMMENT '插件描述',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否启用',
  `type` varchar(50) DEFAULT NULL COMMENT '插件类型 payment支付 login 登陆 shipping物流',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `bank_code` text COMMENT '网银配置信息',
  `scene` tinyint(1) DEFAULT '0' COMMENT '使用场景 0PC+手机 1手机 2PC 3APP 4小程序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_plugin
-- ----------------------------
INSERT INTO `snake_plugin` VALUES ('weixin', '微信支付', '1.0', 'IT宇宙人', 'a:4:{i:0;a:4:{s:4:\"name\";s:5:\"appid\";s:5:\"label\";s:20:\"绑定支付的APPID\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:0:\"\";}i:1;a:4:{s:4:\"name\";s:5:\"mchid\";s:5:\"label\";s:9:\"商户号\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:0:\"\";}i:2;a:4:{s:4:\"name\";s:3:\"key\";s:5:\"label\";s:18:\"商户支付密钥\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:0:\"\";}i:3;a:4:{s:4:\"name\";s:9:\"appsecret\";s:5:\"label\";s:57:\"公众帐号secert（仅JSAPI支付的时候需要配置)\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:0:\"\";}}', 'a:4:{s:5:\"appid\";s:18:\"wx74e103796ecf44b0\";s:5:\"mchid\";s:10:\"1502325671\";s:3:\"key\";s:32:\"0Cg8FQpshdpJ3MTQN1OuCmVRlLoVQgX3\";s:9:\"appsecret\";s:0:\"\";}', 'PC端+微信公众号支付', '1', 'payment', 'logo.jpg', null, '0');

-- ----------------------------
-- Table structure for snake_recharge
-- ----------------------------
DROP TABLE IF EXISTS `snake_recharge`;
CREATE TABLE `snake_recharge` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `nickname` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `order_sn` varchar(30) NOT NULL DEFAULT '' COMMENT '充值单号',
  `account` decimal(10,2) DEFAULT '0.00' COMMENT '充值金额',
  `ctime` int(11) DEFAULT NULL COMMENT '充值时间',
  `pay_time` int(11) DEFAULT NULL COMMENT '支付时间',
  `pay_code` varchar(20) DEFAULT NULL,
  `pay_name` varchar(80) DEFAULT NULL COMMENT '支付方式',
  `pay_status` tinyint(1) DEFAULT '0' COMMENT '充值状态0:待支付 1:充值成功 2:交易关闭',
  `buy_vip` tinyint(1) DEFAULT '0' COMMENT '是否为VIP充值，1是',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_recharge
-- ----------------------------
INSERT INTO `snake_recharge` VALUES ('144', '2879', '老杨', 'rechargetav5k5tawF', '100.00', '1528029380', null, null, null, '0', '0');
INSERT INTO `snake_recharge` VALUES ('146', '2879', '老杨', 'recharge7uNzHA4LTz', '100.00', '1528029503', null, null, null, '0', '0');
INSERT INTO `snake_recharge` VALUES ('147', '2879', '老杨', 'rechargekwrC4E7Bbj', '100.00', '1528029519', null, 'weixin', '微信支付', '0', '0');
INSERT INTO `snake_recharge` VALUES ('148', '2879', '老杨', 'rechargenrgftzriH5', '1.00', '1528032053', null, null, null, '0', '0');
INSERT INTO `snake_recharge` VALUES ('149', '2879', '老杨', 'rechargeEmaCH6qSyi', '100.00', '1528032071', null, null, null, '0', '0');
INSERT INTO `snake_recharge` VALUES ('150', '2879', '老杨', 'rechargeLCkvzPhoyx', '50.00', '1528032204', null, 'weixin', '微信支付', '0', '0');
INSERT INTO `snake_recharge` VALUES ('151', '2879', '老杨', 'recharger9H3okcSdQ', '0.01', '1528035435', '1528039210', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('152', '2879', '老杨', 'rechargeEGfEH9Dqb1', '0.01', '1528038478', '1528040535', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('153', '2879', '老杨', 'rechargeFzbQ8Mqcel', '0.01', '1528039307', '1528039322', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('154', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('155', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('156', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('157', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('158', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('159', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('160', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('161', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('162', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('163', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('164', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('165', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('166', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('170', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('171', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('172', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('173', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('174', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('175', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('176', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('177', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('178', '2879', '老杨', 'rechargeq8ttSizMCr', '0.01', '1528294959', '1528294978', 'weixin', '微信支付', '1', '0');
INSERT INTO `snake_recharge` VALUES ('179', '2880', '男人的好', 'rechargeT5M7cu3nQH', '50.00', '1529417140', null, 'weixin', '微信支付', '0', '0');
INSERT INTO `snake_recharge` VALUES ('180', '2880', '男人的好', 'rechargebqyEnN8Ewb', '50.00', '1529417165', null, 'weixin', '微信支付', '0', '0');
INSERT INTO `snake_recharge` VALUES ('181', '2880', '男人的好', 'rechargevfDGsv8mDq', '50.00', '1530275566', '1530275607', 'weixin', '微信支付', '1', '0');

-- ----------------------------
-- Table structure for snake_role
-- ----------------------------
DROP TABLE IF EXISTS `snake_role`;
CREATE TABLE `snake_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_name` varchar(155) NOT NULL COMMENT '角色名称',
  `rule` varchar(255) DEFAULT '' COMMENT '权限节点数据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of snake_role
-- ----------------------------
INSERT INTO `snake_role` VALUES ('1', '超级管理员', '*');
INSERT INTO `snake_role` VALUES ('2', '系统维护员', '11,12,13,14,39,40,42,43,44,45,46,47,48,49,19,20,21,22,23,24,25,26,27,28,29,30,31,33,34,50,51,35,36,37,38,52,53,54');

-- ----------------------------
-- Table structure for snake_sms_log
-- ----------------------------
DROP TABLE IF EXISTS `snake_sms_log`;
CREATE TABLE `snake_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号',
  `session_id` varchar(128) DEFAULT '' COMMENT 'session_id',
  `add_time` int(11) DEFAULT '0' COMMENT '发送时间',
  `code` varchar(10) DEFAULT '' COMMENT '验证码',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '发送状态,1:成功,0:失败',
  `msg` varchar(255) DEFAULT NULL COMMENT '短信内容',
  `scene` int(1) DEFAULT '0' COMMENT '发送场景,1:用户注册,2:找回密码,3:客户下单,4:客户支付,5:商家发货,6:身份验证',
  `error_msg` text COMMENT '发送短信异常内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_sms_log
-- ----------------------------
INSERT INTO `snake_sms_log` VALUES ('98', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527854786', '95566', '0', '验证码95566，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('99', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527854885', '67647', '0', '验证码67647，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('100', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527854968', '42825', '0', '验证码42825，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('101', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527855126', '43434', '0', '验证码43434，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('102', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527855866', '28006', '0', '验证码28006，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('103', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527855883', '81563', '0', '验证码81563，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('104', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527855893', '23481', '0', '验证码23481，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('105', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527855902', '84698', '0', '验证码84698，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('106', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527856621', '67375', '0', '验证码67375，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('107', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527856637', '95175', '0', '验证码95175，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('108', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527856703', '25648', '0', '验证码25648，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('109', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527856704', '14126', '0', '验证码14126，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('110', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857064', '19790', '0', '验证码19790，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('111', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857246', '61889', '0', '验证码61889，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('112', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857247', '94012', '0', '验证码94012，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('113', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857465', '17318', '0', '验证码17318，您正在进行身份验证，打死不要告诉别人哦！', '1', 'include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
INSERT INTO `snake_sms_log` VALUES ('114', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857509', '69584', '0', '验证码69584，您正在进行身份验证，打死不要告诉别人哦！', '1', 'include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
INSERT INTO `snake_sms_log` VALUES ('115', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857567', '95617', '0', '验证码95617，您正在进行身份验证，打死不要告诉别人哦！', '1', 'include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
INSERT INTO `snake_sms_log` VALUES ('116', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857693', '73367', '0', '验证码73367，您正在进行身份验证，打死不要告诉别人哦！', '1', 'include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
INSERT INTO `snake_sms_log` VALUES ('117', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857704', '25630', '0', '验证码25630，您正在进行身份验证，打死不要告诉别人哦！', '1', 'include_once(./vendor/aliyun-php-sdk-core/Config.php): failed to open stream: No such file or directory');
INSERT INTO `snake_sms_log` VALUES ('118', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857749', '65980', '1', '验证码65980，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('119', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857757', '32503', '0', '验证码32503，您正在进行身份验证，打死不要告诉别人哦！', '1', '触发分钟级流控Permits:1. Code: isv.BUSINESS_LIMIT_CONTROL');
INSERT INTO `snake_sms_log` VALUES ('120', '15361698313', 'bv1tptv7s0g09plh16o7v5k9h0', '1527857979', '86221', '1', '验证码86221，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('121', '15826332989', 'bv1tptv7s0g09plh16o7v5k9h0', '1528003372', '24853', '1', '验证码24853，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('122', '15826332989', 'bv1tptv7s0g09plh16o7v5k9h0', '1528003778', '99545', '1', '验证码99545，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('123', '15361698313', '87e77qfos54jn8oncait1k1r30', '1528561026', '41403', '1', '验证码41403，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('124', '15817228386', 'dm47f78dnmpss755bcnq029810', '1528620214', '43549', '1', '验证码43549，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('125', '15817228386', 'dm47f78dnmpss755bcnq029810', '1528620296', '83266', '1', '验证码83266，您正在进行身份验证，打死不要告诉别人哦！', '1', null);
INSERT INTO `snake_sms_log` VALUES ('126', '15361698313', 'tkvitmolg3lp03h49udqdna1u7', '1528646483', '20928', '1', '验证码20928，您正在进行身份验证，打死不要告诉别人哦！', '1', null);

-- ----------------------------
-- Table structure for snake_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `snake_sms_template`;
CREATE TABLE `snake_sms_template` (
  `tpl_id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `sms_sign` varchar(50) NOT NULL DEFAULT '' COMMENT '短信签名',
  `sms_tpl_code` varchar(100) NOT NULL DEFAULT '' COMMENT '短信模板ID',
  `tpl_content` varchar(512) NOT NULL DEFAULT '' COMMENT '发送短信内容',
  `send_scene` varchar(100) NOT NULL DEFAULT '' COMMENT '短信发送场景',
  `add_time` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_sms_template
-- ----------------------------
INSERT INTO `snake_sms_template` VALUES ('26', '电商之家', 'SMS_136300059', '验证码${code}，您正在进行身份验证，打死不要告诉别人哦！', '1', '1527816542');
INSERT INTO `snake_sms_template` VALUES ('27', '电商之家', 'SMS_136300055', '验证码${code}，您正在尝试修改登录密码，请妥善保管账户信息。', '2', '1527816488');

-- ----------------------------
-- Table structure for snake_user
-- ----------------------------
DROP TABLE IF EXISTS `snake_user`;
CREATE TABLE `snake_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '密码',
  `head` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '头像',
  `login_times` int(11) NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `last_login_ip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `real_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '真实姓名',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `role_id` int(11) NOT NULL DEFAULT '1' COMMENT '用户角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of snake_user
-- ----------------------------
INSERT INTO `snake_user` VALUES ('1', 'admin', '84386805f4fa719c7023544210fea50c', '/upload/head/WCZaTd6hpGelHn9VJvqFA821rDtRmLNc.jpeg', '62', '183.14.78.4', '1530325261', 'admin', '1', '1');
INSERT INTO `snake_user` VALUES ('2', 'demo', '84386805f4fa719c7023544210fea50c', '/static/admin/images/profile_small.jpg', '0', '', '0', 'tom', '1', '2');

-- ----------------------------
-- Table structure for snake_user_level
-- ----------------------------
DROP TABLE IF EXISTS `snake_user_level`;
CREATE TABLE `snake_user_level` (
  `level_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `level_name` varchar(30) DEFAULT NULL COMMENT '头衔名称',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '等级必要金额',
  `discount` smallint(4) DEFAULT '0' COMMENT '折扣',
  `describe` varchar(200) DEFAULT NULL COMMENT '头街 描述',
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_user_level
-- ----------------------------
INSERT INTO `snake_user_level` VALUES ('1', '注册会员', '0.00', '100', '注册会员');
INSERT INTO `snake_user_level` VALUES ('2', '铜牌会员', '10000.00', '98', '铜牌会员');
INSERT INTO `snake_user_level` VALUES ('3', '白银会员', '30000.00', '95', '白银会员');
INSERT INTO `snake_user_level` VALUES ('4', '黄金会员', '50000.00', '92', '黄金会员');
INSERT INTO `snake_user_level` VALUES ('5', '钻石会员', '100000.00', '90', '钻石会员');
INSERT INTO `snake_user_level` VALUES ('6', '超级VIP', '200000.00', '88', '超级VIP');

-- ----------------------------
-- Table structure for snake_users
-- ----------------------------
DROP TABLE IF EXISTS `snake_users`;
CREATE TABLE `snake_users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `paypwd` varchar(32) DEFAULT NULL COMMENT '支付密码',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 保密 1 男 2 女',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '生日',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '用户金额',
  `frozen_money` decimal(10,2) DEFAULT '0.00' COMMENT '冻结金额',
  `pay_points` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '消费积分',
  `address_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '默认收货地址',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `qq` varchar(20) DEFAULT '' COMMENT 'QQ',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `oauth` varchar(10) DEFAULT '' COMMENT '第三方来源 wx weibo alipay',
  `openid` varchar(100) DEFAULT NULL COMMENT '第三方唯一标示',
  `unionid` varchar(100) DEFAULT NULL,
  `head_pic` varchar(255) DEFAULT NULL COMMENT '头像',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `email_validated` tinyint(1) unsigned DEFAULT '0' COMMENT '是否验证电子邮箱',
  `nickname` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '第三方返回昵称',
  `level` tinyint(1) DEFAULT '1' COMMENT '会员等级',
  `total_amount` decimal(10,2) DEFAULT '0.00' COMMENT '消费累计额度',
  `is_lock` tinyint(1) DEFAULT '0' COMMENT '是否被锁定冻结',
  `message_mask` tinyint(1) DEFAULT '63' COMMENT '消息掩码',
  `discount` decimal(11,0) NOT NULL DEFAULT '1' COMMENT '会员折扣，默认1不享受',
  `cityname` varchar(50) DEFAULT NULL COMMENT '所在城市',
  `sign` varchar(255) DEFAULT NULL COMMENT '签名',
  `token` varchar(255) NOT NULL DEFAULT '' COMMENT '用于用户身份识别',
  PRIMARY KEY (`user_id`),
  KEY `openid` (`openid`),
  KEY `unionid` (`unionid`)
) ENGINE=InnoDB AUTO_INCREMENT=2882 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of snake_users
-- ----------------------------
INSERT INTO `snake_users` VALUES ('2878', 'e3d9d4222261a59cc34203e2d94ab684', null, '1', '0', '31.00', '0.00', '100', '0', '1527863145', '1530233754', '183.14.78.4', '', '15361698313', '', null, null, '/upload/avatar/20180626/f5b8dc186c2c37e5407f5d5e2cf562cb.png', '1437935165@qq.com', '0', '这个杀手不太冷', '1', '0.00', '0', '63', '1', '深圳', '电商平台发布会', '3b976aeab6af175a571d7275e9fd0f21');
INSERT INTO `snake_users` VALUES ('2879', 'e3d9d4222261a59cc34203e2d94ab684', null, '0', '0', '0.04', '0.00', '100', '0', '1528003837', '1528003837', '', '', '15826332989', '', null, null, '/upload/avatar/20180615/20835293476d86cbd347bd4429bb7049.png', '', '0', '老杨', '3', '0.00', '0', '63', '1', null, null, '');
INSERT INTO `snake_users` VALUES ('2880', '8c0c143955c78e203afcc99604eb667b', null, '0', '0', '50.00', '0.00', '100', '0', '1528620317', '1530275390', '27.38.41.39', '', '15817228386', '', null, null, '/public/images/user_thumb_empty_300.png', null, '0', '男人的好', '1', '0.00', '0', '63', '1', null, null, '3f3143a39021a6bc252b7d4cfc12d184');
