<?php
namespace app\admin\controller;

use app\admin\model\GoodsModel;
use think\Db;

class Goods extends Base
{
	public function index(){
		if(IS_AJAX){
            $param = input('param.');

            $limit = $param['pageSize'];

            $offset = ($param['pageNumber'] - 1) * $limit;

            $where['status'] = ['neq',3];
            if(!empty($param['express_number'])){
            	$where['express_number'] = $param['express_number'];
            }

            if (!empty($param['searchText'])) {
                $where['goods_name'] = ['like','%'.$param['searchText'].'%'];
            }

            if (!empty($param['pack_id'])) {
            	$where['pack_id'] = $param['pack_id'];
            }

            $pack = new GoodsModel();
            $selectResult = $pack->getDataByWhere($where, $offset, $limit);
            
            $status = config('GOODS_STATUS');
            $express = config('Express');

            

        
            // 拼装参数
            foreach($selectResult as $key=>$vo){
                $user_id = Db::name('pack')->where(['pack_id'=>$vo['pack_id']])->getField('user_id');
                $selectResult[$key]['nickname'] =  Db::name('users')->where(['user_id'=>$user_id])->getField('nickname');
                $selectResult[$key]['goods_origin'] = $vo['goods_origin']?'<img src="'.$vo['goods_origin'].'" style="width:80px;">':'暂无图片';
                $selectResult[$key]['express_code'] = $express[$vo['express_code']];
                $selectResult[$key]['create_time'] = date('Y-m-d H:i:s', $vo['create_time']);
                $selectResult[$key]['status'] = $status[$vo['status']];
                $selectResult[$key]['operate'] = showOperate($this->makeButton($vo));
            }



            $return['total'] = $pack->getAllData($where);  //总数据
            $return['rows'] = $selectResult;

            return json($return);
        }
        $pack_id = input('id/d',0);
        $this->assign('pack_id',$pack_id);
        return $this->fetch();
	}

    //商品备注
    public function note(){
        $goods_id = input('goods_id/d',0);
        $note = input('note/s','');
        Db::name('goods')->where(['goods_id'=>$goods_id])->update(['note'=>$note]);
        return json(msg(0,$note,'操作成功'));
    }

    //商品确认
    public function setAction(){
        $data = htmlspecialchars_decode(input('datalist')); 
        $data = json_decode($data,1);
        foreach ($data as $key => $value) {
            $ids[] = $value['goods_id'];
        }
        // 启动事务
        Db::startTrans();
        try{
            Db::name('goods')->where('goods_id in ('.implode(',', $ids).')')->setField('status',1);
            // 提交事务
            Db::commit();    
            return json(msg(1,'','操作成功'));
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(msg(0,'',$e->getMessage()));
        } 
        
    }

	/**
     * 拼装操作按钮
     * @param $vo
     * @return array
     */
    private function makeButton($vo)
    {
        return [
            // '查看商品' => [
            //     'auth' => 'goods/index',
            //     'href' => url('goods/index', ['id' => $id]),
            //     'btnStyle' => 'primary',
            //     'icon' => 'fa fa-paste'
            // ],
            '不符' => [
                'auth' => 'users/accountlog',
                'href' => "javascript:setIncompatible(" .$vo['goods_id'] .")",
                'btnStyle' => 'primary',
                'icon' => 'fa fa-paste'
            ],
            '查看面单' => [
                'auth' => 'pack/index',
                'href' => url('pack/index', ['id' => $vo['pack_id']]),
                'btnStyle' => 'primary',
                'icon' => 'fa fa-search-minus'
            ]
        ];
    }
}