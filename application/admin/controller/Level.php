<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\LevelModel;

class Level extends Base
{
    //等级
    public function index()
    {
        
    	if(IS_AJAX){

            $param = input('param.');

            $limit = $param['pageSize'];
            $offset = ($param['pageNumber'] - 1) * $limit;

            $where = array();
            $level = new LevelModel();
            $selectResult = $level->getDataByWhere($where, $offset, $limit);

            // 拼装参数
            foreach($selectResult as $key=>$vo){
                $selectResult[$key]['operate'] = showOperate($this->makeButton($vo['level_id']));
            }



            $return['total'] = $level->getCount($where);  //总数据
            $return['rows'] = $selectResult;

            return json($return);
        }

        return $this->fetch();
    }


    //添加
    public function level_add(){
    	if(IS_POST){

            $param = input('post.');
            $user = new LevelModel();
            $flag = $user->insertLevel($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $level = new LevelModel();
        $this->assign([
            'level' => $level->getLevel(),
        ]);

        return $this->fetch();
    }

    //编辑
    public function level_edit(){
    	$level = new LevelModel();

        if(IS_POST){

            $param = input('post.');
            $flag = $level->editLevel($param);
            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }
        $id = input('param.id');
        $this->assign([
            'info' => $level->getOneLevel($id),
        ]);
        return $this->fetch();
    }

    //删除
    public function level_del(){
    	$id = input('id');
        $level = new LevelModel();
        $flag = $level->delLevel($id);
        return json(msg($flag['code'], $flag['data'], $flag['msg']));
    }

    /**
     * 拼装操作按钮
     * @param $id
     * @return array
     */
    private function makeButton($id)
    {
        return [
            '编辑' => [
                'auth' => 'level/level_edit',
                'href' => url('level/level_edit', ['id' => $id]),
                'btnStyle' => 'primary',
                'icon' => 'fa fa-paste'
            ],
            '删除' => [
                'auth' => 'level/level_del',
                'href' => "javascript:levelDel(" .$id .")",
                'btnStyle' => 'danger',
                'icon' => 'fa fa-trash-o'
            ]
        ];
    }

}
