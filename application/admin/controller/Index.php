<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\admin\model\NodeModel;
use think\Db;
class Index extends Base
{
    public function index()
    {
        // 获取权限菜单
        $node = new NodeModel();

        $this->assign([
            'menu' => $node->getMenu(session('rule'))
        ]);

        return $this->fetch('/index');
    }

    /**
     * 后台默认首页
     * @return mixed
     */
    public function indexPage()
    {
        //最近30天ip记录    
        $visitor = Db::name('visitor')->field('count(*) as count,create_time')->whereTime('create_time','-30 day')->group('create_time')->cache(3600)->select();
        // 初始化数据
        foreach($visitor as $key=>$vo){
            $xx[$key] = date('Y-m-d',$vo['create_time']);
            $yy[$key] = $vo['count'];
            $dateTime = date('Y-m-d',$vo['create_time']+86400);
            $xd[$key] = Db::name('pack')->where('pack_status',3)->whereTime('update_time','between',[$xx[$key],$dateTime])->count();
        }

        //访问浏览器类型数据
        $visitorBrower = Db::name('visitor')->field('count(*) as count,browser')->group('browser')->cache(3600)->select();
        foreach ($visitorBrower as $key => $value) {
            $browser[] = $value['browser'];
            $browserObj[$key]['name'] = $value['browser'];
            $browserObj[$key]['value'] = $value['count'];
        }

        $visitorIP = Db::name('visitor')->field('count(*) as count,ip,city')->group('city')->cache(3600)->select();
        foreach ($visitorIP as $key => $value) {
            if (isset($value['city'])&&!empty($value['city'])&&$value['city']!='XX'&&$value['city']!='内网IP') {
                $str[] = "{name:'".$value['city']."',value:10".'}';
            }
            
        }
        $cityObj = '['.implode(',', $str).']';




        //今日ip访问量
        $todayIPCount = Db::name('visitor')->whereTime('create_time', 'today')->cache(3600)->count();
        //总ip量
        $IPCount = Db::name('visitor')->count();
        //今日新增用户
        $todayUserCount = Db::name('users')->whereTime('reg_time', 'today')->cache(3600)->count();
        //用户总数
        $UserCount = Db::name('users')->count();
        //待签收
        $waitSignCount = Db::name('pack')->where('pack_status','eq',1)->cache(3600)->count();
        //已签收
        $SignCount = Db::name('pack')->whereBetween('pack_status','2,3')->whereTime('update_time', 'today')->cache(3600)->count();
        //待发货
        $waitSendCount = Db::name('pack')->where('pack_status','eq',2)->whereTime('update_time', 'today')->cache(3600)->count();
        //已发货
        $SendCount = Db::name('pack')->where('pack_status','eq',3)->whereTime('update_time', 'today')->cache(3600)->count();
    
        $this->assign([
            'xx' => json_encode($xx),
            'yy' => json_encode($yy),
            'xd' => json_encode($xd),
            'todayIPCount' => json_encode($todayIPCount),
            'IPCount' => json_encode($IPCount),
            'browserObj' => json_encode($browserObj),
            'browser' => json_encode($browser),
            'cityObj' => $cityObj,
            'todayUserCount' => $todayUserCount,
            'UserCount' => $UserCount,
            'waitSignCount' => $waitSignCount,
            'SignCount' => $SignCount,
            'SendCount' => $SendCount,
            'waitSendCount' => $waitSendCount,
            
        ]);

        return $this->fetch('index');
    }

    //城市初始化 更新没有城市名称的ip记录
    public function cityInit(){
        $cityList = Db::name('visitor')->where(['city'=>''])->select();
        foreach ($cityList as $key => $value) {
            $cityInfo = findCityByIp($value['ip']);
            Db::name('visitor')->where(['id'=>$value['id']])->update(['country'=>$cityInfo['data']['country'],'region'=>$cityInfo['data']['region'],'city'=>$cityInfo['data']['city']]);
           
        }
        $count = M('visitor')->where('country','eq','')->count();
        $this->ajaxReturn(['status'=>1,'msg'=>'请求成功','count'=>$count]);

    }
}
