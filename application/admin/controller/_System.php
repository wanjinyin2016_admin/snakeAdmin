<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

class System extends Base
{
    //用户列表
    public function index()
    {
		$inc_type =  I('get.inc_type','sms');
		$config = tpCache($inc_type);
		$this->assign('config',$config);//当前配置项
		return $this->fetch($inc_type);
    }

    
}
