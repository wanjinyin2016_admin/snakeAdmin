<?php

/**
 * @Author: 网名
 * @Date:   2018-06-01 09:56:23
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-01 21:45:30
 */
namespace app\admin\controller;
use app\admin\model\SystemModel;
use think\Cache;
/**
*系统设置
*/
class System extends Base
{
 	/*
	 * 配置入口
	 */
	public function index()
	{
		/*配置列表*/
		$group_list = [
            'shop_info' => '网站信息',
            'sms'       => '短信设置',
            'smtp'     => '邮箱设置',
            // 'shopping'  => '购物流程设置',
            // 'smtp'      => '邮件设置',
            // 'water'     => '水印设置',
            // 'distribut' => '分销设置',
            // 'push'      => '推送设置',
            // 'oss'       => '对象存储',
            // 'express'	=> '物流设置'
        ];		
		$this->assign('group_list',$group_list);
		$inc_type =  input('param.inc_type','shop_info');
		$this->assign('inc_type',$inc_type);
		$where['inc_type'] = $inc_type;
		$system = new SystemModel();
		$res = $system->getConfigByWhere($where);
		if($res){
            foreach($res as $k=>$val){
                $config[$val['name']] = $val['value'];
            }
            cache($inc_type,$config,TEMP_PATH);
        }
		$this->assign([
            'config' => $config,
            'status' => config('status'),
        ]);
		return $this->fetch($inc_type);
	}


	 //发送测试邮件
	  public function send_email(){
	    	$param = input('post.');
	    	// tpCache($param['inc_type'],$param);
	    	// unset($param['inc_type']);
	    	$res = send_email($param['test_eamil'],'后台测试','测试发送验证码:'.mt_rand(1000,9999));
	    	exit(json_encode($res));
	  }


	/*
	 * 新增修改配置
	 */
	public function handle()
	{
		$param = input('param.');
		$inc_type = $param['inc_type'];
		unset($param['inc_type']);
		$system = new SystemModel();
		$flag = $system->addEditConfig($param,$inc_type);
		return json(msg($flag['code'], $flag['data'], $flag['msg']));
	}        


	 /**
     * 清空系统缓存
     */
    public function cleanCache(){          
		delFile(RUNTIME_PATH);
		Cache::clear();
		$quick = input('param.quick',0);
		if($quick == 1){
			exit(json_encode(['status'=>1,'msg'=>'缓存清除成功']));
		}else{
			exit(json_encode(['status'=>2,'msg'=>'缓存清除成功']));
		}
       	
    }


	
}
