<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\NavModel;

class Nav extends Base
{
    //导航
    public function index()
    {
        if(IS_AJAX){

            $param = input('param.');

            $limit = $param['pageSize'];
            $offset = ($param['pageNumber'] - 1) * $limit;
            $user = new NavModel();
            $where = [];
            $selectResult = $user->getNavByWhere($where, $offset, $limit);
            foreach ($selectResult as $key => $value) {
                $selectResult[$key]['operate'] = showOperate($this->makeButton($value['id']));
                $selectResult[$key]['is_show'] = $value['is_show']?'<span style="color:#18a689">显示</span>':'<span style="color:#ddd">隐藏</span>';
                $selectResult[$key]['is_new'] = $value['is_new']?'<span style="color:#18a689">是</span>':'<span style="color:#ddd">否</span>';
            }

            $return['total'] = $user->getAllNav($where);  //总数据
            $return['rows'] = $selectResult;

            return json($return);
        }

        return $this->fetch();
    }

    // 添加
    public function add()
    {
        if(request()->isPost()){

            $param = input('post.');
            $user = new NavModel();
            $flag = $user->insertNav($param);
            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }
        return $this->fetch();
    }

    // 编辑
    public function edit()
    {
        $nav = new NavModel();

        if(request()->isPost()){

            $param = input('post.');

            if(empty($param['password'])){
                unset($param['password']);
            }else{
                $param['password'] = md5($param['password'] . config('salt'));
            }
            $flag = $nav->editNav($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $id = input('param.id');

        $this->assign([
            'nav' => $nav->getOneNav($id),
        ]);
        return $this->fetch();
    }

    // 删除
    public function del()
    {
        $id = input('param.id');

        $Nav = new NavModel();
        $flag = $Nav->delNav($id);
        return json(msg($flag['code'], $flag['data'], $flag['msg']));
    }

    /**
     * 拼装操作按钮
     * @param $id
     * @return array
     */
    private function makeButton($id)
    {
        return [
            '编辑' => [
                'auth' => 'nav/edit',
                'href' => url('nav/edit', ['id' => $id]),
                'btnStyle' => 'primary',
                'icon' => 'fa fa-paste'
            ],
            '删除' => [
                'auth' => 'nav/del',
                'href' => "javascript:del(" .$id .")",
                'btnStyle' => 'danger',
                'icon' => 'fa fa-trash-o'
            ]
        ];
    }
}
