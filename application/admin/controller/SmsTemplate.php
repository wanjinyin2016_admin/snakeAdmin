<?php
/**
 *  短信平台短信模板管理
 */
namespace app\admin\controller; 
use app\admin\model\SmsTemplateModel;

class SmsTemplate extends Base {

    public  $send_scene;
    
    public function _initialize() {
        // 短信使用场景
        $this->send_scene = config('SEND_SCENE');
        $this->assign('send_scene', $this->send_scene);
        
    }
    
    public function index(){
        if(IS_AJAX){
            $param = input('param.');

            $limit = $param['pageSize'];
            $offset = ($param['pageNumber'] - 1) * $limit;

            $where = [];
            $SmsTemplateModel = new SmsTemplateModel();
            $selectResult = $SmsTemplateModel->getSmsTemplateByWhere($where, $offset, $limit);

              // 拼装参数
            foreach($selectResult as $key=>$vo){

                $selectResult[$key]['add_time'] = date('Y-m-d H:i:s', $vo['add_time']);
                $selectResult[$key]['send_scene'] = $this->send_scene[$vo['send_scene']][0];
                $selectResult[$key]['operate'] = showOperate($this->makeButton($vo['tpl_id']));
            }

            $return['total'] = $SmsTemplateModel->getSmsTemplate($where);  //总数据
            $return['rows'] = $selectResult;

            return json($return);
        }
        return $this->fetch();
       
    }


     /**
     * 添加  短信模板
     */
    public  function addSmsTemplate(){
        
        $SmsTemplateModel = new SmsTemplateModel();

        if(IS_POST){
            $param = input('post.');

            $param['add_time'] = time();
            $flag = $SmsTemplateModel->addSmsTemplate($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $scenes = $SmsTemplateModel->getSmsTemplateField();
        $scenes = get_arr_column($scenes,'send_scene');
        $filterSendscene = array();
        //过滤已经添加过滤的短信模板
        foreach ($this->send_scene as $key => $value){
            if(!in_array($key, $scenes)){
                $filterSendscene[$key] = $value;
            }
        }
        $this->assign([
            'send_scene' => $filterSendscene,
        ]);
        return $this->fetch();
    }
    
    /**
     * 编辑  短信模板
     */
    public  function editSmsTemplate(){
        
        $SmsTemplateModel = new SmsTemplateModel();

        if(IS_POST){
            $param = input('post.');

            $param['add_time'] = time();
            $flag = $SmsTemplateModel->editSmsTemplate($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $id = input('param.tpl_id');
        $this->assign([
            'smsTemplate' => $SmsTemplateModel->getOneSmsTemplate($id),
            'send_scene' => $this->send_scene,
        ]);
        return $this->fetch();
    }

    
    
    /**
     * 删除模版
     */
   public function delTemplate(){
       
       $model = M("sms_template");
       $row = $model->where('tpl_id ='.$_GET['id'])->delete();
       $return_arr = array();
       if ($row){
           $return_arr = array('status' => 1,'msg' => '删除成功','data'  =>'',);   //$return_arr = array('status' => -1,'msg' => '删除失败','data'  =>'',);
       }else{
           $return_arr = array('status' => -1,'msg' => '删除失败','data'  =>'',);  
       } 
       return $this->ajaxReturn($return_arr);
       
   }

   /**
     * 拼装操作按钮
     * @param $id
     * @return array
     */
    private function makeButton($id)
    {
        return [
            '编辑' => [
                'auth' => 'sms_template/edit_sms_template',
                'href' => url('SmsTemplate/editSmsTemplate', ['tpl_id' => $id]),
                'btnStyle' => 'primary',
                'icon' => 'fa fa-paste'
            ],
            '删除' => [
                'auth' => 'sms_template/del_template',
                'href' => "javascript:delTemplateDel(" .$id .")",
                'btnStyle' => 'danger',
                'icon' => 'fa fa-trash-o'
            ]
        ];
    }

}