<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class LevelModel extends Model
{
    // 确定链接表名
    protected $name = 'user_level';

    /**
     * 根据搜索条件获取角色列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getDataByWhere($where, $offset, $limit)
    {

        return $this->where($where)->limit($offset, $limit)->order('level_id asc')->select();
    }

    public function getLevel(){
        return $this->select();
    }

    /**
     * 根据搜索条件获取所有的角色数量
     * @param $where
     */
    public function getCount($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 插入等级信息
     * @param $param
     */
    public function insertLevel($param)
    {
        try{

            $result =  $this->validate('LevelValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('level/index'), '添加等级成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑等级信息
     * @param $param
     */
    public function editLevel($param)
    {
        try{

            $result = $this->validate('LevelValidate')->save($param, ['level_id' => $param['level_id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('level/index'), '编辑等级成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据用户id获取用户信息
     * @param $id
     */
    public function getOneLevel($id)
    {
        return $this->where('level_id', $id)->find();
    }


    /**
     * 删除等级
     * @param $id
     */
    public function delLevel($id)
    {
        try{

            $this->where('level_id', $id)->delete();
            return msg(1, '', '删除等级成功');

        }catch(PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    
}
