<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class NavModel extends Model
{
    // 确定链接表名
    protected $name = 'nav';

    /**
     * 根据搜索条件获取角色列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getNavByWhere($where, $offset, $limit)
    {
        return $this->where($where)->limit($offset, $limit)->order('id asc')->select();
    }

    /**
     * 根据搜索条件获取所有的导航
     * @param $where
     */
    public function getAllNav($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 插入导航信息
     * @param $param
     */
    public function insertLevel($param)
    {
        try{

            $result =  $this->validate('NavValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('level/index'), '添加等级成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑导航信息
     * @param $param
     */
    public function editNav($param)
    {
        try{

            $result = $this->validate('NavValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('Nav/index'), '编辑导航成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据id获取菜单信息
     * @param $id
     */
    public function getOneNav($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * 删除导航
     * @param $id
     */
    public function delNav($id)
    {
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除导航成功');

        }catch(PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

}
