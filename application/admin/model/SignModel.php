<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class SignModel extends Model
{
    // 确定链接表名
    protected $name = 'user_sign';

    /**
     * 根据搜索条件获取用户列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getDataByWhere($where, $offset, $limit)
    {
        return $this->alias('user_sign')->field( 'user_sign.*,nickname,mobile')
            ->join('users', 'user_sign.user_id = ' . 'users.user_id')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取所有的用户数量
     * @param $where
     */
    public function getCount($where)
    {
        return $this->where($where)->count();
    }




}
