<?php

/**
 * @Author: 网名
 * @Date:   2018-06-03 10:21:46
 * @Last Modified by:   网名
 * @Last Modified time: 2018-06-03 14:55:54
 */
namespace app\admin\model;

use think\Model;

class PluginModel extends Model
{
    // 确定链接表名
    protected $name = 'plugin';

     /**
     * 根据搜索条件获取插件列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getPluginByWhere($where, $offset, $limit)
    {
        return $this->where($where)->limit($offset, $limit)->order('code desc')->select();
    }

     /**
     * 根据搜索条件获取所有的用户数量
     * @param $where
     */
    public function getAllPlugin($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 插入系统配置信息
     * @param $param
     */
    public function inOrunstallPlugin($status,$type,$code)
    {

        
        try{
            $newArr = array('status'=>$status);
         	if ($status==1) {
         		$result = $this->save($newArr,['type'=>$type,'code'=>$code]);
         	}else{
         		$result = $user->where(['type'=>$type,'code'=>$code])->delete();
         	}
            
            return msg(1, url('System/index',array('inc_type'=>$inc_type)), '配置更新成功');
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }


    public function insertPlugin($param){
    	try{
            $result =  $this->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('plugin/index'), '添加用户成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }
}