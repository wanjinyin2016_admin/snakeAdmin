<?php

/**
 * @Author: Administrator
 * @Date:   2018-05-31 22:20:11
 * @Last Modified by:   Administrator
 * @Last Modified time: 2018-06-01 09:01:23
 */
namespace app\admin\model;

use think\Model;

class SmsTemplateModel extends Model
{
	// 确定链接表名
    protected $name = 'sms_template';

    /**
     * 根据搜索条件获取短信模版列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getSmsTemplateByWhere($where, $offset, $limit)
    {
        return $this->where($where)->limit($offset, $limit)->order('tpl_id desc')->select();
    }

     /**
     * 根据搜索条件获取所有的短信模版数量
     * @param $where
     */
    public function getSmsTemplate($where)
    {
        return $this->where($where)->count();
    }

     /**
     * 根据模版id获短信模版信息
     * @param $id
     */
    public function getOneSmsTemplate($id)
    {
        return $this->where('tpl_id', $id)->find();
    }


 	/**
     * 编辑短信模版信息
     * @param $param
     */
    public function editSmsTemplate($param)
    {

        try{
            $result =  $this->validate('SmsTemplateValidate')->save($param, ['tpl_id' => $param['tpl_id']]);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('smsTemplate/index'), '编辑短信模版成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑短信模版信息
     * @param $param
     */
    public function addSmsTemplate($param)
    {

        try{
            $result =  $this->validate('SmsTemplateValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('smsTemplate/index'), '编辑短信模版成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 获取短信 指定字段 信息
     * @return [type] [description]
     */
    public function getSmsTemplateField(){
    	return $this->field('send_scene')->select();
    }





}