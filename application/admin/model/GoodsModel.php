<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class GoodsModel extends Model
{
    // 确定链接表名
    protected $name = 'goods';

    /**
     * 根据搜索条件获取数据表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getDataByWhere($where, $offset, $limit)
    {
        
        return $this->where($where)->limit($offset, $limit)->order('goods_id desc')->select();
    }

    /**
     * 根据搜索条件获取所有记录数量
     * @param $where
     */
    public function getAllData($where)
    {
        
        return $this->where($where)->count();
    }
}
