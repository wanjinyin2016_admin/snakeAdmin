<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class SmsTemplateValidate extends Validate
{
    protected $rule = [
        ['sms_sign', 'require', '应用场景已经存在'],
        ['sms_tpl_code', 'unique:sms_template', '应用场景已经存在'],
        ['send_scene', 'unique:sms_template', '应用场景已经存在'],
        ['tpl_content', 'require', '应用场景已经存在']
    ];

}