<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class UsersValidate extends Validate
{
    protected $rule = [
        ['mobile', 'unique:users', '手机号码已经存在'],
        ['nickname', 'require', '昵称不能为空'],
        // ['password', 'require', '密码不能为空'],
        ['level', 'require', '请选择用户等级']

    ];

}