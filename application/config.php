<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id$

return [
    // 是否开启路由
    'url_route_on' => true,
    'trace' => [
        'type' => 'html', // 支持 socket trace file
    ],
    //各模块公用配置
    'extra_config_list' => ['database', 'route', 'validate'],
    //临时关闭日志写入
    'log'                    => [
        // 日志记录方式，支持 file socket
        'type' => 'File',
        // 日志保存目录
        'path' => LOG_PATH,
        'LOG_RECORD_LEVEL'  => ['error','notice' ],
    ],
    
    'app_debug' => true,

     // 默认全局过滤方法 用逗号分隔多个
    'default_filter' => 'htmlspecialchars',

    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------
    'cache' => [
        // 驱动方式
        'type' => 'file',
        // 缓存保存目录
        'path' => CACHE_PATH,
        // 缓存前缀
        'prefix' => '',
        // 缓存有效期 0表示永久缓存
        'expire' => 0,
    ],


    //短信使用场景
    'SEND_SCENE' => array(
        '1'=>array('用户注册','验证码${code}，您正在进行身份验证，打死不要告诉别人哦！','regis_sms_enable'),
        '2'=>array('用户找回密码','验证码${code}，您正在尝试修改登录密码，请妥善保管账户信息。','forget_pwd_sms_enable'),
        '3'=>array('面单签收','尊敬的用户，您面单编号为：${pack_number}（已签收）' ),
        '4'=>array('面单发货','尊敬的用户，您面单编号为：${pack_number}（已发货）' )
    ),

    // 密码加密串
    'AUTH_CODE' => "LAZADA100", //安装完毕之后不要改变，否则所有密码都会出错

    //面单状态
    //0未提交 待提交
    // 1已提交 待确认
    // 2已确认 待签收
    // 3已签收 待扫描
    // 4已扫描 待完成
    // 5已完成
    // 5已作废
    'PACK_STATUS'=>array(
        '0'=>'未确认',
        '1'=>'已确认',
        '2'=>'已签收',
        '3'=>'已发货',
        '4'=>'已完成',
        '5'=>'已作废',
    ),

    //商品状态
    'GOODS_STATUS'=>array(
        '0'=>'待确认',
        '1'=>'已确认',
        '2'=>'丢失',  //物流包里面没有对应的商品
        '3'=>'不符',  ///物流包裹里面的商品与上传商品不符
    ),
    //支付状态
    'PAY_STATUS'=>array(
        '0'=>'未支付',
        '1'=>'已支付',
    ),
    'Express'=>array(
        'SF'=>'顺丰',
        'HTKY'=>'百世快递',
        'ZTO'=>'中通快递',
        'STO'=>'申通快递',
        'YTO'=>'圆通速递',
        'YD'=>'韵达速递',
        'YZPY'=>'邮政快递包裹',
        'EMS'=>'EMS',
        'HHTT'=>'天天快递',
        'JD'=>'京东物流',
        'UC'=>'优速快递',
        'DBL'=>'德邦快递',
        'FEDEX'=>'FEDEX联邦(国内件）',
        'FEDEX_GJ'=>'FEDEX联邦(国际件）',
        'FAST'=>'快捷快递',
        'ZJS'=>'宅急送',
        'TNT'=>'TNT快递',
        'UPS'=>'UPS',
        'DHL'=>'DHL'
    ),
    'platform'=>[
        '1'=>['name'=>'Lazada','free'=>'3'],
        '2'=>['name'=>'Jumia','free'=>'5'],
        '3'=>['name'=>'kilimall','free'=>'3'],
        '4'=>['name'=>'Shopee','free'=>'3'],
        '5'=>['name'=>'Wish','free'=>'3'],
    ],

    //加密串
    'salt' => 'wZPb~yxvA!ir38&Z',
    //备份数据地址
    'back_path' => APP_PATH .'../back/'
];
