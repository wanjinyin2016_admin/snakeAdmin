# snakeAdmin

#### 项目介绍
电商服务系统

#### 软件架构


```
|-- application     // 项目业务逻辑主要目录
    |-- admin       // 后台所在目录
       |-- controller       // 后台控制器
       |-- model        // 后台模型
       |-- validate         // 后台验证器
       |-- view          // 后台视图
       |-- common.php    // 后台公用方法
       |-- config.php    // 后台项目配置
       |-- database.php  // 后台数据库配置
    |-- api     // 规划 api 目录
    |-- index   // 前台所在目录
        |-- controller       // 前台控制器
        |-- model        // 前台模型
        |-- validate         // 前台验证器
        |-- view          // 前台视图
        |-- common.php    // 前台公用方法
        |-- config.php    // 前台项目配置
        |-- database.php  // 前台数据库配置
    |-- common   // 公共目录
        |-- logic         // 逻辑类
        |-- model         // 公共模型
        |-- validate      // 公共验证器
    |-- command.php     // console命令文件
    |-- common.php      // 系统公用方法
    |-- config.php      // 系统配置文件
    |-- database.php    // 系统数据库文件
    |-- route.php       // 系统路由文件
|-- back    // sql备份文件目录
|-- extend      // 自由扩展包目录 
|-- public      // 系统入口、资源所在目录
    |-- static      // 静态资源
        |-- admin   // 后台资源目录
```



#### 安装教程


```
1. 删除 /snakeAdmin/data/install.lock 锁定文件
2. 删除 /snakeAdmin/application/database.php 数据库配置文件
3. 访问 http://您的域名 根据提示完成snakeAdmin安装
4. 后台账号密码  admin 123456
```


#### 使用说明


```
1. 前端是仿layui官网
2. 后台模块：用户管理 权限管理 会员管理 会员等级 短信模版  短信配置  网站设置 文章管理  数据库备份
3. 配置nginx伪静态
    location / {
    	if (!-e $request_filename){
    		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
    	}
    }
```

#### 参与贡献


```
1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
```



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)