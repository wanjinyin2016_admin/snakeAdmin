<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:70:"/www/wwwroot/snakeAdmin/public/../application/index/view/user/set.html";i:1532170108;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/header.html";i:1536494124;s:73:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/left.html";i:1529503258;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/footer.html";i:1535898493;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>帐号设置 - <?php echo $tp_config['shop_info_store_name']; ?></title>
        <link rel="stylesheet" href="__STATIC__/index/css/font.css">
        <link rel="stylesheet" href="__STATIC__/index/layui/css/layui.css?t=1527238724161">
        <link rel="stylesheet" href="__STATIC__/index/css/global.css" charset="utf-8">
    </head>
    <body>
        <div class="fly-header layui-bg-black">
    <div class="layui-container"> 
        <a class="fly-logo" href="/"> <!-- <img src="__STATIC__/index/img/logo.png" alt="layui"> --> </a>
        <ul class="layui-nav fly-nav layui-hide-xs">
            <?php if(is_array($nav) || $nav instanceof \think\Collection || $nav instanceof \think\Paginator): if( count($nav)==0 ) : echo "" ;else: foreach($nav as $key=>$vo): ?>
                <li class="layui-nav-item"> <a href="<?php echo $vo['url']; ?>" <?php if($vo['is_new'] == 1): ?>target="_black"<?php endif; ?>><i class="layui-icon <?php echo $vo['icon']; ?>"></i><?php echo $vo['name']; ?></a> </li> 
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <?php if($user): ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item ">
                <a class="fly-nav-avatar" href="<?php echo url('User/index'); ?>" id="LAY_header_avatar">
                    <cite class="layui-hide-xs"><?php echo $user['nickname']; ?></cite>
                    <img src="<?php echo (isset($user['head_pic']) && ($user['head_pic'] !== '')?$user['head_pic']:'/static/index/img/avatar.png'); ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('User/index'); ?>">
                            <i class="layui-icon">&#xe612;</i>
                            用户中心
                        </a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('user/set'); ?>">
                            <i class="layui-icon">&#xe620;</i>
                            基本设置
                        </a>
                    </dd>
                    <!-- <hr> -->
                 <!--    <dd>
                        <a href="/user/message/">
                            <i class="iconfont icon-tongzhi" style="top: 4px;"></i>
                            我的消息
                        </a>
                    </dd>
                    <dd>
                        <a href="/u/16471056">
                            <i class="layui-icon" style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>
                            我的主页
                        </a>
                    </dd> -->
                    <hr style="margin: 5px 0;">
                    <dd>
                        <a href="<?php echo url('User/logout'); ?>" style="text-align: center;">退出</a>
                    </dd>
                </dl>
            </li>
        </ul>
        <?php else: ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item"> <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('User/login'); ?>"></a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/login'); ?>">登入</a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/register'); ?>">注册</a> </li>
            
           <!--  <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a> </li>
            <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a> </li> -->
        </ul>
        <?php endif; ?>
       
    </div>
</div>
        <div class="layui-container fly-marginTop fly-user-main">
            <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
    <li class="layui-nav-item">
        <a href="<?php echo url('Main/index',array('id'=>$user['user_id'])); ?>">
            <i class="layui-icon">&#xe68e;</i>
            我的主页 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'index'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/index'); ?>">
            <i class="layui-icon">&#xe612;</i>
            用户中心 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'set'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/set'); ?>">
            <i class="layui-icon">&#xe620;</i>
            基本设置 
        </a>
    </li>
   <!--  <li class="layui-nav-item ">
        <a href="/user/post/">
            <i class="iconfont icon-tiezi"></i>
            我的帖子 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'message'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/message'); ?>">
            <i class="layui-icon">&#xe611;</i>
            我的消息 
        </a>
    </li> -->
    <li class="layui-nav-item <?php if($controller == 'Pack'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('pack/packList'); ?>">
            <i class="iconfont icon-chanpin"></i>
            打包记录 
        </a>
    </li>
</ul>
            <div class="site-tree-mobile layui-hide">
                <i class="layui-icon">&#xe602;</i>
            </div>
            <div class="site-mobile-shade"></div>
            <div class="fly-panel fly-panel-user" pad20>
                <div class="layui-tab layui-tab-brief" lay-filter="user">
                    <ul class="layui-tab-title" id="LAY_mine">
                        <li class="layui-this" lay-id="info">我的资料</li>
                        <li lay-id="avatar">头像</li>
                        <li lay-id="pass">密码</li>
                        <!-- <li lay-id="bind">帐号绑定</li> -->
                    </ul>
                    <div class="layui-tab-content" style="padding: 20px 0;">
                        <div class="layui-form layui-form-pane layui-tab-item layui-show">
                            <form method="post">
                                <div class="layui-form-item">
                                    <label for="L_email" class="layui-form-label">手机</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="L_phone" name="phone" required lay-verify="phone" autocomplete="off" value="<?php echo $user['mobile']; ?>" class="layui-input" disabled style="cursor: not-allowed !important;">
                                    </div>
                                    <div class="layui-form-mid layui-word-aux">
                                        <span style="color: #5FB878">您已完成手机号绑定，已正式成为<?php echo $tp_config['shop_info_store_name']; ?>实名用户。</span>
                                        手机号暂不支持修改。 
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label for="L_email" class="layui-form-label">邮箱</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="L_email" name="email" required lay-verify="email" autocomplete="off" value="<?php echo $user['email']; ?>" class="layui-input">
                                    </div>
                                    <div class="layui-form-mid layui-word-aux">
                                        <?php if($user['email']): if($user['email_validated'] != 1): ?>
                                                您的邮箱尚未激活，请：<a href="<?php echo url('user/activate'); ?>" style="color: #4f99cf;">激活邮箱</a>
                                                <?php else: ?>
                                                您的邮箱已激活，也可以作为登入名
                                            <?php endif; else: ?>
                                            邮箱也可以作为登入名，推荐设置
                                        <?php endif; ?>
                                        
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label for="L_username" class="layui-form-label">昵称</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="L_nickname" name="nickname" required lay-verify="required" autocomplete="off" value="<?php echo $user['nickname']; ?>" class="layui-input">
                                    </div>
                                    <div class="layui-inline">
                                        <div class="layui-input-inline" style="width:250px">
                                        	<input type="radio" name="sex" value="0" <?php if($user['sex'] == 0): ?>checked<?php endif; ?>  title="保密">
                                            <input type="radio" name="sex" value="1" <?php if($user['sex'] == 1): ?>checked<?php endif; ?>  title="男">
                                            <input type="radio" name="sex" value="2" <?php if($user['sex'] == 2): ?>checked<?php endif; ?>  title="女">
                                        </div>
                                    </div>
                                </div>

                                <div class="layui-form-item"> 
                                    <label for="L_city" class="layui-form-label">城市</label> 
                                    <div class="layui-input-inline"> 
                                        <input type="text" id="L_city" name="cityname" autocomplete="off" value="<?php echo $user['cityname']; ?>" class="layui-input"> 
                                    </div> 
                                </div>
                                
                                <div class="layui-form-item layui-form-text">
                                    <label for="L_sign" class="layui-form-label">签名</label>
                                    <div class="layui-input-block">
                                        <textarea placeholder="随便写些什么刷下存在感" id="L_sign" name="sign" autocomplete="off" class="layui-textarea" style="height: 80px;"><?php echo $user['sign']; ?></textarea>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <button class="layui-btn" key="set-mine" lay-filter="*" lay-submit>确认修改</button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-form layui-form-pane layui-tab-item">
                            <div class="layui-form-item">
                                <div class="avatar-add">
                                    <p>建议尺寸168*168，支持jpg、png、gif，最大不能超过50KB</p>
                                    <button type="button" class="layui-btn upload-img">
                                        <i class="layui-icon">&#xe67c;</i>
                                        上传头像 
                                    </button>
                                    <img src="<?php echo (isset($user['head_pic']) && ($user['head_pic'] !== '')?$user['head_pic']:'/static/index/img/avatar.png'); ?>">
                                    <span class="loading"></span>
                                </div>
                            </div>
                        </div>
                        <div class="layui-form layui-form-pane layui-tab-item">
                            <form action="<?php echo url('User/set'); ?>" method="post">
                                <div class="layui-form-item">
                                    <label for="L_pass" class="layui-form-label">新密码</label>
                                    <div class="layui-input-inline">
                                        <input type="password" id="L_pass" name="password" required lay-verify="required" autocomplete="off" class="layui-input">
                                    </div>
                                    <div class="layui-form-mid layui-word-aux">6到16个字符</div>
                                </div>
                                <div class="layui-form-item">
                                    <label for="L_repass" class="layui-form-label">确认密码</label>
                                    <div class="layui-input-inline">
                                        <input type="password" id="L_repass" name="repass" required lay-verify="required" autocomplete="off" class="layui-input">
                                    </div>
                                </div> 
                                <div class="layui-form-item">
                                    <button class="layui-btn" key="set-mine" lay-filter="*" lay-submit>确认修改</button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-form layui-form-pane layui-tab-item">
                            <ul class="app-bind">
                                <li class="fly-msg  app-havebind">
                                    <i class="iconfont icon-qq"></i>
                                    <span>已成功绑定，您可以使用QQ帐号直接登录电商之家，当然，您也可以</span>
                                    <a href="javascript:;" class="acc-unbind" type="qq_id">解除绑定</a>
                                </li>
                                <li class="fly-msg ">
                                    <i class="iconfont icon-weibo"></i>
                                    <a href="/app/weibo/" class="acc-weibo" type="weibo_id" onclick="layer.msg('正在绑定微博', {icon:16, shade: 0.1, time:0})">立即绑定</a>
                                    <span>，即可使用微博帐号登录电商之家</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="fly-footer">
    <p><a href=""><?php echo $tp_config['shop_info_store_name']; ?></a> 2018 &copy; <a href="">lazada100.com</a></p>
    <p> 
        <a href="" target="_blank">付费计划</a> 
        <a href="/case/2018/" target="_blank">商标版权</a> 
        <a href="" target="_blank">联系我们</a> 
        <a href="" target="_blank">微信公众号</a> 
    </p>
    <p class="fly-union"> 
        <a href="" target="_blank" rel="nofollow" upyun><img src="__STATIC__/index/img/upyun.png?t=1"></a> 
        <span>提供 CDN 赞助</span> 
    </p>
</div>
<ul class="layui-fixbar">
    <li class="layui-icon" lay-type="bar1" style="background-color:#009688"></li>
    <li class="layui-icon layui-fixbar-top" lay-type="top" style="background-color: rgb(0, 150, 136); display: list-item;"></li>
</ul>
<script src="__STATIC__/common/js/global.js"></script>
<script src="__STATIC__/index/layui/layui.js"></script>
<script src="__STATIC__/index/js/index.js"></script>
<script>
    var SMSURL = "<?php echo url('index/api/send_validate_code'); ?>";
    var REGISTERURL = "<?php echo url('Index/User/register'); ?>"; 
    var VERIFYURL = "<?php echo url('vertify',array('type'=>'repwd')); ?>?tm="+Math.random();
    var VERCODEURL = "<?php echo url('Api/checkVercode',array('type'=>'repwd')); ?>?tm="+Math.random();
</script>
<script src="__STATIC__/index/js/user.js"></script>
</body>
</html>
