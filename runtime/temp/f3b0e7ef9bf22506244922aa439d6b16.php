<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:72:"/www/wwwroot/snakeAdmin/public/../application/index/view/user/index.html";i:1532611432;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/header.html";i:1536494124;s:73:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/left.html";i:1529503258;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/footer.html";i:1535898493;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>用户中心 - <?php echo $tp_config['shop_info_store_name']; ?></title>
    <link rel="stylesheet" href="__STATIC__/index/css/font.css">
    <link rel="stylesheet" href="__STATIC__/index/layui/css/layui.css?t=1527238724161">
    <link rel="stylesheet" href="__STATIC__/index/css/global.css" charset="utf-8">
</head>
<script type="text/javascript">
    var ajaxGetStatus = "<?php echo url('User/signStatus'); ?>";
    var ajaxSignIn = "<?php echo url('User/signIn'); ?>";
    </script>
<body>
    <div class="fly-header layui-bg-black">
    <div class="layui-container"> 
        <a class="fly-logo" href="/"> <!-- <img src="__STATIC__/index/img/logo.png" alt="layui"> --> </a>
        <ul class="layui-nav fly-nav layui-hide-xs">
            <?php if(is_array($nav) || $nav instanceof \think\Collection || $nav instanceof \think\Paginator): if( count($nav)==0 ) : echo "" ;else: foreach($nav as $key=>$vo): ?>
                <li class="layui-nav-item"> <a href="<?php echo $vo['url']; ?>" <?php if($vo['is_new'] == 1): ?>target="_black"<?php endif; ?>><i class="layui-icon <?php echo $vo['icon']; ?>"></i><?php echo $vo['name']; ?></a> </li> 
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <?php if($user): ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item ">
                <a class="fly-nav-avatar" href="<?php echo url('User/index'); ?>" id="LAY_header_avatar">
                    <cite class="layui-hide-xs"><?php echo $user['nickname']; ?></cite>
                    <img src="<?php echo (isset($user['head_pic']) && ($user['head_pic'] !== '')?$user['head_pic']:'/static/index/img/avatar.png'); ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('User/index'); ?>">
                            <i class="layui-icon">&#xe612;</i>
                            用户中心
                        </a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('user/set'); ?>">
                            <i class="layui-icon">&#xe620;</i>
                            基本设置
                        </a>
                    </dd>
                    <!-- <hr> -->
                 <!--    <dd>
                        <a href="/user/message/">
                            <i class="iconfont icon-tongzhi" style="top: 4px;"></i>
                            我的消息
                        </a>
                    </dd>
                    <dd>
                        <a href="/u/16471056">
                            <i class="layui-icon" style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>
                            我的主页
                        </a>
                    </dd> -->
                    <hr style="margin: 5px 0;">
                    <dd>
                        <a href="<?php echo url('User/logout'); ?>" style="text-align: center;">退出</a>
                    </dd>
                </dl>
            </li>
        </ul>
        <?php else: ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item"> <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('User/login'); ?>"></a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/login'); ?>">登入</a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/register'); ?>">注册</a> </li>
            
           <!--  <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a> </li>
            <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a> </li> -->
        </ul>
        <?php endif; ?>
       
    </div>
</div>
    <div class="layui-container fly-marginTop fly-user-main">
        <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
    <li class="layui-nav-item">
        <a href="<?php echo url('Main/index',array('id'=>$user['user_id'])); ?>">
            <i class="layui-icon">&#xe68e;</i>
            我的主页 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'index'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/index'); ?>">
            <i class="layui-icon">&#xe612;</i>
            用户中心 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'set'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/set'); ?>">
            <i class="layui-icon">&#xe620;</i>
            基本设置 
        </a>
    </li>
   <!--  <li class="layui-nav-item ">
        <a href="/user/post/">
            <i class="iconfont icon-tiezi"></i>
            我的帖子 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'message'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/message'); ?>">
            <i class="layui-icon">&#xe611;</i>
            我的消息 
        </a>
    </li> -->
    <li class="layui-nav-item <?php if($controller == 'Pack'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('pack/packList'); ?>">
            <i class="iconfont icon-chanpin"></i>
            打包记录 
        </a>
    </li>
</ul>
        <div class="site-tree-mobile layui-hide"> <i class="layui-icon">&#xe602;</i></div>
        <div class="site-mobile-shade"></div>
        <div class="fly-panel fly-panel-user" pad20 style="padding-top:20px;">
            <?php if($user['email_validated'] == 0): ?>
            <div class="fly-msg" style="margin-bottom: 20px;"> 
                您的邮箱尚未验证，这比较影响您的帐号安全，
                <a href="<?php echo url('index/user/activate'); ?>">立即去激活？</a> 
            </div>
            <?php endif; ?>
            <div class="layui-row layui-col-space20">
                <div class="layui-col-md6">
                    <div class="fly-panel fly-signin fly-panel-border">
                        <div class="fly-panel-title"> 签到 
                            <i class="fly-mid"></i> 
                            <a href="javascript:;" class="fly-link" id="LAY_signinHelp">说明</a> 
                           <!--  <i class="fly-mid"></i> 
                            <a href="javascript:;" class="fly-link" id="LAY_signinTop">活跃榜<span class="layui-badge-dot"></span></a>  -->
                            <span class="fly-signin-days"></span>
                        </div>
                        <div class="fly-panel-main fly-signin-main"> <i class="layui-icon fly-loading">&#xe63d;</i> </div>
                    </div>
                </div>
                <div class="layui-col-md6">
                    <div class="fly-panel fly-panel-border">
                        <div class="fly-panel-title"> 我的会员信息
                            <i class="fly-mid"></i> 
                            <a href="javascript:;" class="fly-link" id="LAY_signinHelp">等级</a> 
                            <i class="fly-mid"></i> 
                            <a href="<?php echo url('User/recharge'); ?>" class="fly-link" id="LAY_RECHARGE">充值</a> 
                        </div>
                        <div class="fly-panel-main layui-text" style="padding: 18px 15px; height: 50px; line-height: 26px;">
                            <div class="layui-row">
                                <div class="layui-col-md4"><p>账户余额：￥<?php echo $user['user_money']; ?></p></div>
                                <div class="layui-col-md4"><p>消费积分：<?php echo $user['pay_points']; ?></p></div>
                                <div class="layui-col-md4"><p>冻结金额：￥<?php echo $user['frozen_money']; ?></p></div>
                            </div>
                            <div class="layui-row">
                                <div class="layui-col-md4"><p>会员等级：<?php echo $user['level']; ?></p></div>
                            </div>
                            
                            
                            
                        </div>
                    </div>
                </div>
                <div class="layui-col-md12" style="margin-top: -20px;">
                    <div class="fly-panel fly-panel-border">
                        <div class="fly-panel-title"> 快捷方式 </div>
                        <div class="fly-panel-main">
                            <ul class="layui-row layui-col-space10 fly-shortcut">
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="/index/pack/packadd"><i class="layui-icon">&#xe61f;</i><cite>新增打包</cite></a> </li>
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="<?php echo url('User/recharge'); ?>"><i class="layui-icon">&#xe65e;</i><cite>余额充值</cite></a> </li>
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="<?php echo url('user/accountLog'); ?>"><i class="layui-icon">&#xe62d;</i><cite>资金明细</cite></a> </li>
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="/jie/2461/"><i class="layui-icon">&#xe63a;</i><cite>关注公众号</cite></a> </li>
                                
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="/index/user/set/"><i class="layui-icon">&#xe620;</i><cite>修改信息</cite></a> </li>
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="/index/user/set/#avatar"><i class="layui-icon">&#xe6af;</i><cite>修改头像</cite></a> </li>
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="/index/user/set/#pass"><i class="layui-icon">&#xe607;</i><cite>修改密码</cite></a> </li>
                                <li class="layui-col-sm3 layui-col-xs4"> <a href="http://www.layui.com/doc/"><i class="layui-icon">&#xe63c;</i><cite>文档</cite></a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fly-footer">
    <p><a href=""><?php echo $tp_config['shop_info_store_name']; ?></a> 2018 &copy; <a href="">lazada100.com</a></p>
    <p> 
        <a href="" target="_blank">付费计划</a> 
        <a href="/case/2018/" target="_blank">商标版权</a> 
        <a href="" target="_blank">联系我们</a> 
        <a href="" target="_blank">微信公众号</a> 
    </p>
    <p class="fly-union"> 
        <a href="" target="_blank" rel="nofollow" upyun><img src="__STATIC__/index/img/upyun.png?t=1"></a> 
        <span>提供 CDN 赞助</span> 
    </p>
</div>
<ul class="layui-fixbar">
    <li class="layui-icon" lay-type="bar1" style="background-color:#009688"></li>
    <li class="layui-icon layui-fixbar-top" lay-type="top" style="background-color: rgb(0, 150, 136); display: list-item;"></li>
</ul>
<script src="__STATIC__/common/js/global.js"></script>
<script src="__STATIC__/index/layui/layui.js"></script>
<script src="__STATIC__/index/js/index.js"></script>
<script>
    var SMSURL = "<?php echo url('index/api/send_validate_code'); ?>";
    var REGISTERURL = "<?php echo url('Index/User/register'); ?>"; 
    var VERIFYURL = "<?php echo url('vertify',array('type'=>'repwd')); ?>?tm="+Math.random();
    var VERCODEURL = "<?php echo url('Api/checkVercode',array('type'=>'repwd')); ?>?tm="+Math.random();
</script>
    <script src="__STATIC__/index/js/user.js"></script>
</body>

</html>