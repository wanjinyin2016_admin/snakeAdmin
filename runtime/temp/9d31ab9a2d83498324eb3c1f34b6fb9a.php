<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:72:"/www/wwwroot/snakeAdmin/public/../application/index/view/user/login.html";i:1533084718;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/header.html";i:1536494124;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/footer.html";i:1535898493;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>登入 - 电商之家</title>
    <link rel="stylesheet" href="__STATIC__/index/css/font.css">
    <link rel="stylesheet" href="__STATIC__/index/layui/css/layui.css?t=1527238724161">
    <link rel="stylesheet" href="__STATIC__/index/css/global.css" charset="utf-8">
</head>

<body>
    <div class="fly-header layui-bg-black">
    <div class="layui-container"> 
        <a class="fly-logo" href="/"> <!-- <img src="__STATIC__/index/img/logo.png" alt="layui"> --> </a>
        <ul class="layui-nav fly-nav layui-hide-xs">
            <?php if(is_array($nav) || $nav instanceof \think\Collection || $nav instanceof \think\Paginator): if( count($nav)==0 ) : echo "" ;else: foreach($nav as $key=>$vo): ?>
                <li class="layui-nav-item"> <a href="<?php echo $vo['url']; ?>" <?php if($vo['is_new'] == 1): ?>target="_black"<?php endif; ?>><i class="layui-icon <?php echo $vo['icon']; ?>"></i><?php echo $vo['name']; ?></a> </li> 
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <?php if($user): ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item ">
                <a class="fly-nav-avatar" href="<?php echo url('User/index'); ?>" id="LAY_header_avatar">
                    <cite class="layui-hide-xs"><?php echo $user['nickname']; ?></cite>
                    <img src="<?php echo (isset($user['head_pic']) && ($user['head_pic'] !== '')?$user['head_pic']:'/static/index/img/avatar.png'); ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('User/index'); ?>">
                            <i class="layui-icon">&#xe612;</i>
                            用户中心
                        </a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('user/set'); ?>">
                            <i class="layui-icon">&#xe620;</i>
                            基本设置
                        </a>
                    </dd>
                    <!-- <hr> -->
                 <!--    <dd>
                        <a href="/user/message/">
                            <i class="iconfont icon-tongzhi" style="top: 4px;"></i>
                            我的消息
                        </a>
                    </dd>
                    <dd>
                        <a href="/u/16471056">
                            <i class="layui-icon" style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>
                            我的主页
                        </a>
                    </dd> -->
                    <hr style="margin: 5px 0;">
                    <dd>
                        <a href="<?php echo url('User/logout'); ?>" style="text-align: center;">退出</a>
                    </dd>
                </dl>
            </li>
        </ul>
        <?php else: ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item"> <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('User/login'); ?>"></a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/login'); ?>">登入</a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/register'); ?>">注册</a> </li>
            
           <!--  <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a> </li>
            <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a> </li> -->
        </ul>
        <?php endif; ?>
       
    </div>
</div>
    <div class="layui-container fly-marginTop">
        <div class="fly-panel fly-panel-user" pad20>
            <div class="layui-tab layui-tab-brief" lay-filter="user">
                <ul class="layui-tab-title">
                    <li class="layui-this">登入</li>
                    <li><a href="<?php echo url('User/register'); ?>">注册</a></li>
                </ul>
                <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                    <div class="layui-tab-item layui-show">
                        <div class="layui-form layui-form-pane">
                            <form method="post">
                                <div class="layui-form-item">
                                    <label for="L_email" class="layui-form-label">手机/邮箱</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="L_loginName" name="loginName" required lay-verify="required" autocomplete="off" class="layui-input"> </div>
                                    <div class="layui-form-mid layui-word-aux"> 使用手机或者邮箱中的任意一个均可（若采用手机，请确保你的帐号已绑定过该手机） </div>
                                </div>
                                <div class="layui-form-item">
                                    <label for="L_pass" class="layui-form-label">密码</label>
                                    <div class="layui-input-inline">
                                        <input type="password" id="L_pass" name="pass" required lay-verify="required" autocomplete="off" class="layui-input"> </div>
                                </div>
                                <div class="layui-form-item">
                                    <label for="L_vercode" class="layui-form-label">人类验证</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="L_vercode" name="vercode" required lay-verify="required" placeholder="请收入后面" autocomplete="off" class="layui-input"> 
                                    </div>
                                    <div class="layui-input-inline"> <img id="verify"  src="<?php echo url('User/vertify'); ?>" alt="" width="120px"> </div>
                                </div>
                                <div class="layui-form-item">
                                    <button class="layui-btn" lay-filter="login" lay-submit>立即登录</button> <span style="padding-left:20px;"> <a href="<?php echo url('user/forget'); ?>">忘记密码？</a> </span> </div>
                                <!-- <div class="layui-form-item fly-form-app"> <span>或者使用社交账号登入</span> <a href="/app/qq" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-qq" title="QQ登入"></a> <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-weibo" title="微博登入"></a> </div> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fly-footer">
    <p><a href=""><?php echo $tp_config['shop_info_store_name']; ?></a> 2018 &copy; <a href="">lazada100.com</a></p>
    <p> 
        <a href="" target="_blank">付费计划</a> 
        <a href="/case/2018/" target="_blank">商标版权</a> 
        <a href="" target="_blank">联系我们</a> 
        <a href="" target="_blank">微信公众号</a> 
    </p>
    <p class="fly-union"> 
        <a href="" target="_blank" rel="nofollow" upyun><img src="__STATIC__/index/img/upyun.png?t=1"></a> 
        <span>提供 CDN 赞助</span> 
    </p>
</div>
<ul class="layui-fixbar">
    <li class="layui-icon" lay-type="bar1" style="background-color:#009688"></li>
    <li class="layui-icon layui-fixbar-top" lay-type="top" style="background-color: rgb(0, 150, 136); display: list-item;"></li>
</ul>
<script src="__STATIC__/common/js/global.js"></script>
<script src="__STATIC__/index/layui/layui.js"></script>
<script src="__STATIC__/index/js/index.js"></script>
<script>
    var SMSURL = "<?php echo url('index/api/send_validate_code'); ?>";
    var REGISTERURL = "<?php echo url('Index/User/register'); ?>"; 
    var VERIFYURL = "<?php echo url('vertify',array('type'=>'repwd')); ?>?tm="+Math.random();
    var VERCODEURL = "<?php echo url('Api/checkVercode',array('type'=>'repwd')); ?>?tm="+Math.random();
</script>
    <script>
        var VERIFYURL = "<?php echo url('vertify'); ?>?tm="+Math.random();
        var LOGINURL = "<?php echo url('User/login'); ?>";



        
      
    </script>
</body>

</html>