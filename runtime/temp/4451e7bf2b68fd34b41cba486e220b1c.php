<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/user/recharge.html";i:1532138034;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/header.html";i:1536494124;s:73:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/left.html";i:1529503258;s:75:"/www/wwwroot/snakeAdmin/public/../application/index/view/public/footer.html";i:1535898493;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<title>余额充值 - <?php echo $tp_config['shop_info_store_name']; ?></title>
    <link rel="stylesheet" href="__STATIC__/index/css/font.css">
    <link rel="stylesheet" href="__STATIC__/index/layui/css/layui.css?t=1527238724161">
    <link rel="stylesheet" href="__STATIC__/index/css/global.css" charset="utf-8">
</head>
<style type="text/css" media="screen">
	#input_val{
		display: none;
		width: 160px;
	    position: absolute;
	    top: 0px;
	    left: 520px;
	}
</style>
<body>
	<div class="fly-header layui-bg-black">
    <div class="layui-container"> 
        <a class="fly-logo" href="/"> <!-- <img src="__STATIC__/index/img/logo.png" alt="layui"> --> </a>
        <ul class="layui-nav fly-nav layui-hide-xs">
            <?php if(is_array($nav) || $nav instanceof \think\Collection || $nav instanceof \think\Paginator): if( count($nav)==0 ) : echo "" ;else: foreach($nav as $key=>$vo): ?>
                <li class="layui-nav-item"> <a href="<?php echo $vo['url']; ?>" <?php if($vo['is_new'] == 1): ?>target="_black"<?php endif; ?>><i class="layui-icon <?php echo $vo['icon']; ?>"></i><?php echo $vo['name']; ?></a> </li> 
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <?php if($user): ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item ">
                <a class="fly-nav-avatar" href="<?php echo url('User/index'); ?>" id="LAY_header_avatar">
                    <cite class="layui-hide-xs"><?php echo $user['nickname']; ?></cite>
                    <img src="<?php echo (isset($user['head_pic']) && ($user['head_pic'] !== '')?$user['head_pic']:'/static/index/img/avatar.png'); ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('User/index'); ?>">
                            <i class="layui-icon">&#xe612;</i>
                            用户中心
                        </a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('user/set'); ?>">
                            <i class="layui-icon">&#xe620;</i>
                            基本设置
                        </a>
                    </dd>
                    <!-- <hr> -->
                 <!--    <dd>
                        <a href="/user/message/">
                            <i class="iconfont icon-tongzhi" style="top: 4px;"></i>
                            我的消息
                        </a>
                    </dd>
                    <dd>
                        <a href="/u/16471056">
                            <i class="layui-icon" style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>
                            我的主页
                        </a>
                    </dd> -->
                    <hr style="margin: 5px 0;">
                    <dd>
                        <a href="<?php echo url('User/logout'); ?>" style="text-align: center;">退出</a>
                    </dd>
                </dl>
            </li>
        </ul>
        <?php else: ?>
        <ul class="layui-nav fly-nav-user">
            <li class="layui-nav-item"> <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('User/login'); ?>"></a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/login'); ?>">登入</a> </li>
            <li class="layui-nav-item"> <a href="<?php echo url('User/register'); ?>">注册</a> </li>
            
           <!--  <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a> </li>
            <li class="layui-nav-item layui-hide-xs"> <a href="#" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a> </li> -->
        </ul>
        <?php endif; ?>
       
    </div>
</div>
	<div class="layui-container fly-marginTop fly-user-main">
            <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
    <li class="layui-nav-item">
        <a href="<?php echo url('Main/index',array('id'=>$user['user_id'])); ?>">
            <i class="layui-icon">&#xe68e;</i>
            我的主页 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'index'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/index'); ?>">
            <i class="layui-icon">&#xe612;</i>
            用户中心 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'set'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/set'); ?>">
            <i class="layui-icon">&#xe620;</i>
            基本设置 
        </a>
    </li>
   <!--  <li class="layui-nav-item ">
        <a href="/user/post/">
            <i class="iconfont icon-tiezi"></i>
            我的帖子 
        </a>
    </li>
    <li class="layui-nav-item <?php if($action == 'message'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('User/message'); ?>">
            <i class="layui-icon">&#xe611;</i>
            我的消息 
        </a>
    </li> -->
    <li class="layui-nav-item <?php if($controller == 'Pack'): ?>layui-this<?php endif; ?>">
        <a href="<?php echo url('pack/packList'); ?>">
            <i class="iconfont icon-chanpin"></i>
            打包记录 
        </a>
    </li>
</ul>
            <div class="site-tree-mobile layui-hide">
                <i class="layui-icon">&#xe602;</i>
            </div>
            <div class="site-mobile-shade"></div>
            <div class="fly-panel fly-panel-user" pad20>
                <div class="layui-tab layui-tab-brief" lay-filter="recharge">
                    <ul class="layui-tab-title">
                        <li class="layui-this" lay-id="recharge">余额充值</li>
                        <li lay-id="rechargeLog">充值记录</li>
                        <li lay-id="accountLog">资金明细</li>
                        <!-- <li lay-id="bind">提现记录</li> -->
                    </ul>
                    <div class="layui-tab-content" style="padding: 20px 0;">
                        <form method="post"  id="recharge_form" action="<?php echo url('user/recharge'); ?>" class="layui-form">
                            <div class="layui-form-item">
                                <label class="layui-form-label">产品：</label>
                                <div class="layui-form-mid" style="float: none;">
                                    <a href="JavaScript:;" target="_blank">平台余额充值</a>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">充值金额：</label>
                                <div class="layui-input-block layui-recharge-item">
                                    <button type="button" class="fly-form-btn layui-btn layui-btn-primary layui-this" data-money="50" data-type="change" data-required="true">￥50</button>
                                    <button type="button" class="fly-form-btn layui-btn layui-btn-primary" data-money="100" data-type="change" data-required="true">￥100</button>
                                    <button type="button" class="fly-form-btn layui-btn layui-btn-primary" data-money="200" data-type="change" data-required="true">￥200</button>
                                    <span type="button" class="fly-form-btn layui-btn layui-btn-primary " id="input_val_btn" onkeyup="changeRecharge()">输入金额</span>
                                    
                                    <input type="text" name="account" class="layui-input" value="50" id="input_val" onkeyup="this.value=this.value.replace(/\D/g,'')">
                                </div>
                            </div>
                            
                      
                            <div class="layui-form-item">
                                <label class="layui-form-label">所需费用：</label>
                                <div class="layui-input-block">
                                    <div class="layui-form-mid" id="LAY_price" style="font-size: 23px; color: #FF5722;">￥50</div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">支付方式：</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="pay_radio" value="pay_code=weixin" title="" checked>
                                    <div lay-radio>
                                        <i class="iconfont icon-weixinzhifu" title="微信" style="top: -1px; font-size: 28px"></i>
                                    </div>
                                    <input type="radio" name="pay_radio" value="pay_code=alipay" title="" disabled="1">
                                    <div lay-radio>
                                        <i class="iconfont icon-alipay" title="支付宝" style="top: -1px;x"></i>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <input type="checkbox" name="agreement" lay-skin="primary" title="" checked>
                                    <a href="javascript:;" id="lazada-bill-agreement" style="position: relative; top: 6px; left: 5px; color: #5FB878;">同意产品服务协议</a>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn layui-btn-danger" lay-submit lay-filter="orderPay">立即提交</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="fly-footer">
    <p><a href=""><?php echo $tp_config['shop_info_store_name']; ?></a> 2018 &copy; <a href="">lazada100.com</a></p>
    <p> 
        <a href="" target="_blank">付费计划</a> 
        <a href="/case/2018/" target="_blank">商标版权</a> 
        <a href="" target="_blank">联系我们</a> 
        <a href="" target="_blank">微信公众号</a> 
    </p>
    <p class="fly-union"> 
        <a href="" target="_blank" rel="nofollow" upyun><img src="__STATIC__/index/img/upyun.png?t=1"></a> 
        <span>提供 CDN 赞助</span> 
    </p>
</div>
<ul class="layui-fixbar">
    <li class="layui-icon" lay-type="bar1" style="background-color:#009688"></li>
    <li class="layui-icon layui-fixbar-top" lay-type="top" style="background-color: rgb(0, 150, 136); display: list-item;"></li>
</ul>
<script src="__STATIC__/common/js/global.js"></script>
<script src="__STATIC__/index/layui/layui.js"></script>
<script src="__STATIC__/index/js/index.js"></script>
<script>
    var SMSURL = "<?php echo url('index/api/send_validate_code'); ?>";
    var REGISTERURL = "<?php echo url('Index/User/register'); ?>"; 
    var VERIFYURL = "<?php echo url('vertify',array('type'=>'repwd')); ?>?tm="+Math.random();
    var VERCODEURL = "<?php echo url('Api/checkVercode',array('type'=>'repwd')); ?>?tm="+Math.random();
</script>
        <script src="__STATIC__/index/js/user.js"></script>
        
</body>
</html>